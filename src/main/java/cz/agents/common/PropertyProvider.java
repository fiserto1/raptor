package cz.agents.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyProvider {

    private Properties properties;
    private static final String PROPERTIES_FILEPATH = "application.properties";
    private static PropertyProvider instance = null;

    private PropertyProvider() {
        try {
            this.properties =  loadProperties();
        } catch (IOException e) {
            throw new RuntimeException("Cannot load properties from file.", e);
        }
    }

    public static PropertyProvider getInstance() {
        if (instance != null) {
            return instance;
        } else {
            return new PropertyProvider();
        }
    }

    private Properties loadProperties() throws IOException {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String filepath = rootPath + PROPERTIES_FILEPATH;

        Properties appProps = new Properties();
        appProps.load(new FileInputStream(filepath));

        return appProps;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
