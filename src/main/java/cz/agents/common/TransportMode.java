package cz.agents.common;

public enum TransportMode {
    FOOT(0), BICYCLE(2*60), CAR(10*60), TAXI(15*60), PT(0);

    private int penalization;

    TransportMode(int penalization) {
        this.penalization = penalization;
    }

    public int getTimePenalization() {
        return penalization;
    }

}
