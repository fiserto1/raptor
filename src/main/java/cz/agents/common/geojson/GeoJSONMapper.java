package cz.agents.common.geojson;

import cz.agents.basestructures.GPSLocation;
import cz.agents.common.TransportMode;
import cz.agents.osrm.KPI;
import cz.agents.raptor.Leg;
import cz.agents.raptor.problems.MultiEarliestArrivalProblem;
import cz.agents.raptor.structures.MultiTransfer;
import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Transfer;
import mil.nga.sf.geojson.Feature;
import mil.nga.sf.geojson.FeatureCollection;
import mil.nga.sf.geojson.FeatureConverter;
import mil.nga.sf.geojson.GeoJsonObject;
import mil.nga.sf.geojson.Geometry;
import mil.nga.sf.geojson.Point;
import mil.nga.sf.geojson.Position;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeoJSONMapper {

    public static GeoJsonObject parseStopsToGeoJson(Map<Stop, MultiEarliestArrivalProblem.StopData> stops, int departureDateTime) {
        List<Feature> features = new ArrayList<>();

        for (Map.Entry<Stop, MultiEarliestArrivalProblem.StopData> entry : stops.entrySet()) {
            Stop stop = entry.getKey();
            Position position = new Position(stop.getStopLon(), stop.getStopLat());
            Geometry geometry = new Point(position);
            Feature feature = new Feature(geometry);

            Map<String, Object> properties = new HashMap<>();
            MultiEarliestArrivalProblem.StopData stopData = entry.getValue();

            List<TransportMode> modes = null;
            if (stopData.earliestArrivalTime == stopData.earliestArrivalTimeList[0]) {
                Transfer transfer = stopData.transferList[0];
                if (transfer instanceof MultiTransfer) {
                    modes = ((MultiTransfer) transfer).getModes();
                }

            }
            properties.put("firstMileModes", modes);
            properties.put("numOfTrips", stopData.getTripCount());
            properties.put("eatInS", stopData.earliestArrivalTime - departureDateTime);
            feature.setProperties(properties);
            features.add(feature);
        }

        return new FeatureCollection(features);
    }

    public static GeoJsonObject parseStopsToGeoJson(Map<String, Stop> stops) {
        List<Feature> features = new ArrayList<>();

        for (Map.Entry<String, Stop> entry : stops.entrySet()) {
            Stop stop = entry.getValue();
            Position position = new Position(stop.getStopLon(), stop.getStopLat());
            Geometry geometry = new Point(position);
            Feature feature = new Feature(geometry);

            Map<String, Object> properties = new HashMap<>();

            properties.put("id", stop.getId());
            properties.put("name", stop.getName());
            feature.setProperties(properties);
            features.add(feature);
        }

        return new FeatureCollection(features);
    }

    public static GeoJsonObject parseGPSLocationsToGeoJson(List<GPSLocation> locations) {
        List<Feature> features = new ArrayList<>();

        for (GPSLocation location : locations) {
            Position position = new Position(location.getLongitude(), location.getLatitude());
            Geometry geometry = new Point(position);
            Feature feature = new Feature(geometry);
            features.add(feature);
        }

        return new FeatureCollection(features);
    }

    public static GeoJsonObject parsePlansToGeoJson(List<Plan> plans) {
        List<Feature> features = new ArrayList<>();

        for (Plan plan : plans) {
            if (plan == null) {
                continue;
            }
            
            Stop stop = plan.getTargetStop();
            Position position = new Position(stop.getStopLon(), stop.getStopLat());
            Geometry geometry = new Point(position);

            List<TransportMode> modes = new ArrayList<>();
            for (Leg leg : plan.getLegs()) {
                modes.add(leg.getMode());
            }

            Map<String, Object> properties = new HashMap<>();
            properties.put("usedModes", modes);
            properties.put("numOfTrips", plan.getNumOfTrips());
            properties.put("duration", plan.getDuration());
            properties.put("numOfLegs", plan.getLegs().size());
            properties.put("name", stop.getName());

            Feature feature = new Feature(geometry);
            feature.setProperties(properties);
            features.add(feature);
        }

        return new FeatureCollection(features);
    }

    public static GeoJsonObject parseParetoPlansToGeoJson(Map<Stop, List<Plan>> paretoPlans) {
        List<Feature> features = new ArrayList<>();

        for (Stop stop : paretoPlans.keySet()) {
            List<Plan> plans = paretoPlans.get(stop);
            Position position = new Position(stop.getStopLon(), stop.getStopLat());
            Geometry geometry = new Point(position);

            Map<String, Object> properties = new HashMap<>();
            properties.put("pareto", plans);
            properties.put("name", stop.getName());

            Feature feature = new Feature(geometry);
            feature.setProperties(properties);
            features.add(feature);
        }


        return new FeatureCollection(features);
    }

    public static GeoJsonObject parseLocationsWithKPIToGeoJson(Map<GPSLocation, KPI> kpis) {
        List<Feature> features = new ArrayList<>();

        for (Map.Entry<GPSLocation, KPI> mapEntry : kpis.entrySet()) {
            GPSLocation location = mapEntry.getKey();
            KPI kpi = mapEntry.getValue();

            Position position = new Position(location.getLongitude(), location.getLatitude());
            Geometry geometry = new Point(position);

            Map<String, Object> properties = new HashMap<>();
            properties.put("compoundKPI", kpi.getCompoundKPI());
            properties.put("travelTimeCoverage", kpi.getRangedTravelTimeCoverage());
            properties.put("priceCoverage", kpi.getRangedPriceCoverage());
            properties.put("interchangesCoverage", kpi.getRangedInterchangesCoverage());
            properties.put("combinedCoverage", kpi.getCombinedRangedCoverage());

            Feature feature = new Feature(geometry);
            feature.setProperties(properties);
            features.add(feature);
        }

        return new FeatureCollection(features);
    }

    public static void writeGeoJson(String fileNamePrefix, LocalDateTime timestamp, GeoJsonObject geoJsonObject) {
        String geojsonStringValue = FeatureConverter.toStringValue(geoJsonObject);
        String formattedTimestamp = timestamp.format(
                DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"));
        try (FileWriter wr = new FileWriter(new File(fileNamePrefix + formattedTimestamp + ".geojson"))) {
            wr.write(geojsonStringValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
