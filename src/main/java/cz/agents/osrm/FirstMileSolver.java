package cz.agents.osrm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.agents.basestructures.GPSLocation;
import cz.agents.common.TransportMode;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.KDTree;
import cz.agents.osrm.services.TableOsrmService;
import cz.agents.osrm.structures.Profile;
import cz.agents.raptor.problems.McProblem;
import cz.agents.raptor.problems.MultiEarliestArrivalProblem;
import cz.agents.raptor.structures.MultiTransfer;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Transfer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FirstMileSolver {
    private static final int AVG_SPEED_IN_TOWN = 35;
    private MultiEarliestArrivalProblem eap;

    public FirstMileSolver() {
    }

    public void solve(McProblem problem, KDTree<GPSLocation> kdTree, Map<GPSLocation, List<Stop>> locationStopMap) {
        List<Transfer> firstMileTransfers = new ArrayList<>();
        GPSLocation sourceLocation = problem.getSourceLocation();
        Set<TransportMode> allowedModes = problem.getAllowedModes();
        for (TransportMode allowedMode : allowedModes) {
            double[] sourceCoords = GPSLocationKDTreeResolver.getCoords(sourceLocation);
            double desiredDistance = chooseDesiredSearchRange(allowedMode);
            List<GPSLocation> nearestStopLocations = kdTree.getNearestNodesCloserThan(sourceCoords, desiredDistance);

            Profile profile = chooseOsrmProfile(allowedMode);
            JSONObject fastestRoutes = findOsrmRoutes(sourceLocation, nearestStopLocations, profile);

            List<Transfer> newTransfers = constructFirstMileTransfers(
                    fastestRoutes, nearestStopLocations, allowedMode, problem.getTimetable(), locationStopMap);
            firstMileTransfers.addAll(newTransfers);
        }
        problem.setFirstMileTransfers(firstMileTransfers);
    }

    public void solve(MultiEarliestArrivalProblem eap, KDTree<GPSLocation> kdTree, Map<GPSLocation, List<Stop>> locationStopMap) {
        List<Transfer> firstMileTransfers = new ArrayList<>();
        GPSLocation sourceLocation = eap.getSourceLocation();
        Set<TransportMode> allowedModes = eap.getAllowedModes();
        for (TransportMode allowedMode : allowedModes) {
            double[] sourceCoords = GPSLocationKDTreeResolver.getCoords(sourceLocation);
            double desiredDistance = chooseDesiredSearchRange(allowedMode);
            List<GPSLocation> nearestStopLocations = kdTree.getNearestNodesCloserThan(sourceCoords, desiredDistance);

            Profile profile = chooseOsrmProfile(allowedMode);
            JSONObject fastestRoutes = findOsrmRoutes(sourceLocation, nearestStopLocations, profile);

            List<Transfer> newTransfers = constructFirstMileTransfers(
                    fastestRoutes, nearestStopLocations, allowedMode, eap.getTimetable(), locationStopMap);
            firstMileTransfers.addAll(newTransfers);
        }
        eap.setFirstMileTransfers(firstMileTransfers);
    }

    private JSONObject findOsrmRoutes(GPSLocation source, List<GPSLocation> destinations, Profile profile) {
        TableOsrmService tableOsrmService = new TableOsrmService();

        List<Integer> sourceIds = Collections.singletonList(0);
        String sourceCoord = String.format("%s,%s", source.getLongitude(), source.getLatitude());

        StringBuilder coord = new StringBuilder(sourceCoord);
        List<Integer> destinationIds = new ArrayList<>();
        int id = 1;
        for (GPSLocation destination : destinations) {
            coord.append(";").append(destination.getLongitude()).append(",").append(destination.getLatitude());
            destinationIds.add(id);
            id++;
        }

        JSONObject fastestRoutes = tableOsrmService.generateTravelTimeMatrix(
                coord.toString(), profile.name().toLowerCase(), sourceIds, destinationIds
        );

        if (McMain.SHOULD_LOG) {
            System.out.println("Journeys:");
            if (fastestRoutes != null) {
                System.out.println(fastestRoutes.toString());
            }
        }

        return fastestRoutes;
    }

    private List<Transfer> constructFirstMileTransfers(JSONObject fastestRoutes,
                                                       List<GPSLocation> nearestStopLocations,
                                                       TransportMode mode,
                                                       Timetable timetable,
                                                       Map<GPSLocation, List<Stop>> locationStopMap
    ) {
        if (fastestRoutes == null) {
            System.out.println("First mile route does not exist.");
            return Collections.emptyList();
        }

        if (!fastestRoutes.getString("code").equalsIgnoreCase("OK")) {
            System.out.println("Bad request to OSRM.");
            return Collections.emptyList();
        }

        JSONArray durations = fastestRoutes.getJSONArray("durations").getJSONArray(0);
        ObjectMapper mapper = new ObjectMapper();
        List<Integer> durationsInS = null;
        try {
            durationsInS = mapper.readerFor(new TypeReference<List<Integer>>(){}).readValue(durations.toString());
        } catch (IOException e) {
            System.out.println("Durations cannot be parsed from json response.");
            return Collections.emptyList();
        }

        List<Transfer> firstMileTransfers = new ArrayList<>();

        System.out.println("# of nearest Stops:" + nearestStopLocations.size());
        for (int i = 0; i < nearestStopLocations.size(); i++) {
            GPSLocation nearestStartLocation = nearestStopLocations.get(i);

            List<Stop> stops = locationStopMap.get(nearestStartLocation);
            for (Stop stop : stops) {
                //arrival time: departureDateTime + durationsInS.get(i)
                int transferTime = durationsInS.get(i) + mode.getTimePenalization();
                int price = computePrice(mode, durationsInS.get(i));
                Transfer transfer = new MultiTransfer(null, stop, transferTime, Collections.singletonList(mode), price);
                firstMileTransfers.add(transfer);
            }
        }

        return firstMileTransfers;
    }

    private int computePrice(TransportMode mode, int durationInS) {
        float pricePerMinute = chooseModePricePerMinute(mode);
        float pricePerKm = chooseModePricePerKm(mode);
        int distanceInKm = AVG_SPEED_IN_TOWN * durationInS/3600;
        return (int) (pricePerMinute*durationInS/60 + pricePerKm*distanceInKm);
    }

    private float chooseModePricePerKm(TransportMode mode) {
        switch (mode) {
            case FOOT:
                return 0;
            case BICYCLE:
                return 1;
            case CAR:
                return 0.5f;
            case TAXI:
                return 1.5f;
            default:
                return 0;
        }
    }

    private float chooseModePricePerMinute(TransportMode mode) {
        switch (mode) {
            case FOOT:
                return 0;
            case BICYCLE:
                return 0;
            case CAR:
                return 0;
            case TAXI:
                return 0.5f;
            default:
                return 0;
        }
    }

    private double chooseDesiredSearchRange(TransportMode mode) {
        switch (mode) {
            case FOOT:
                return 1_000;
            case BICYCLE:
                return 4_000;
            case CAR:
                return 8_000;
            case TAXI:
                return 8_000;
            default:
                return 0;
        }
    }

    private Profile chooseOsrmProfile(TransportMode mode) {
        switch (mode) {
            case FOOT:
                return Profile.WALKING;
            case BICYCLE:
                return Profile.CYCLING;
            case CAR:
                return Profile.DRIVING;
            case TAXI:
                return Profile.DRIVING;
            default:
                return null;
        }
    }
}
