package cz.agents.osrm;

public class KPI {
    private float avgBestTravelTime;
    private float avgAvgTravelTime;
    private float avgWorstTravelTime;
    private float avgBestPrice;
    private float avgAvgPrice;
    private float avgWorstPrice;
    private float avgBestNumOfInterchanges;
    private float avgAvgNumOfInterchanges;
    private float rangedTravelTimeCoverage;
    private float rangedPriceCoverage;
    private float rangedInterchangesCoverage;
    private float combinedRangedCoverage;
    private float compoundKPI;

    public float getAvgBestTravelTime() {
        return avgBestTravelTime;
    }

    public void setAvgBestTravelTime(float avgBestTravelTime) {
        this.avgBestTravelTime = avgBestTravelTime;
    }

    public float getAvgAvgTravelTime() {
        return avgAvgTravelTime;
    }

    public void setAvgAvgTravelTime(float avgAvgTravelTime) {
        this.avgAvgTravelTime = avgAvgTravelTime;
    }

    public float getAvgWorstTravelTime() {
        return avgWorstTravelTime;
    }

    public void setAvgWorstTravelTime(float avgWorstTravelTime) {
        this.avgWorstTravelTime = avgWorstTravelTime;
    }

    public float getAvgBestPrice() {
        return avgBestPrice;
    }

    public void setAvgBestPrice(float avgBestPrice) {
        this.avgBestPrice = avgBestPrice;
    }

    public float getAvgAvgPrice() {
        return avgAvgPrice;
    }

    public void setAvgAvgPrice(float avgAvgPrice) {
        this.avgAvgPrice = avgAvgPrice;
    }

    public float getAvgWorstPrice() {
        return avgWorstPrice;
    }

    public void setAvgWorstPrice(float avgWorstPrice) {
        this.avgWorstPrice = avgWorstPrice;
    }

    public float getAvgBestNumOfInterchanges() {
        return avgBestNumOfInterchanges;
    }

    public void setAvgBestNumOfInterchanges(float avgBestNumOfInterchanges) {
        this.avgBestNumOfInterchanges = avgBestNumOfInterchanges;
    }

    public float getAvgAvgNumOfInterchanges() {
        return avgAvgNumOfInterchanges;
    }

    public void setAvgAvgNumOfInterchanges(float avgAvgNumOfInterchanges) {
        this.avgAvgNumOfInterchanges = avgAvgNumOfInterchanges;
    }

    public float getRangedTravelTimeCoverage() {
        return rangedTravelTimeCoverage;
    }

    public void setRangedTravelTimeCoverage(float rangedTravelTimeCoverage) {
        this.rangedTravelTimeCoverage = rangedTravelTimeCoverage;
    }

    public float getRangedPriceCoverage() {
        return rangedPriceCoverage;
    }

    public void setRangedPriceCoverage(float rangedPriceCoverage) {
        this.rangedPriceCoverage = rangedPriceCoverage;
    }

    public float getRangedInterchangesCoverage() {
        return rangedInterchangesCoverage;
    }

    public void setRangedInterchangesCoverage(float rangedInterchangesCoverage) {
        this.rangedInterchangesCoverage = rangedInterchangesCoverage;
    }

    public void setCombinedRangedCoverage(float combinedRangedCoverage) {
        this.combinedRangedCoverage = combinedRangedCoverage;
    }

    public float getCombinedRangedCoverage() {
        return combinedRangedCoverage;
    }

    public float getCompoundKPI() {
        return compoundKPI;
    }

    public void setCompoundKPI(float compoundKPI) {
        this.compoundKPI = compoundKPI;
    }

    @Override
    public String toString() {
        return "KPI{" +
                "avgBestTravelTime=" + avgBestTravelTime +
                ", avgAvgTravelTime=" + avgAvgTravelTime +
                ", avgWorstTravelTime=" + avgWorstTravelTime +
                ", avgBestPrice=" + avgBestPrice +
                ", avgAvgPrice=" + avgAvgPrice +
                ", avgWorstPrice=" + avgWorstPrice +
                ", avgBestNumOfInterchanges=" + avgBestNumOfInterchanges +
                ", avgAvgNumOfInterchanges=" + avgAvgNumOfInterchanges +
                ", rangedTravelTimeCoverage=" + rangedTravelTimeCoverage +
                ", rangedPriceCoverage=" + rangedPriceCoverage +
                ", rangedInterchangesCoverage=" + rangedInterchangesCoverage +
                ", combinedRangedCoverage=" + combinedRangedCoverage +
                ", compoundKPI=" + compoundKPI +
                '}';
    }
}
