package cz.agents.osrm;

import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Stop;

import java.util.List;
import java.util.Map;

public class KpiUtils {

    //WEIGHTS
    public static float TRAVEL_TIME_KPI_WEIGHT = 0.35f;
    public static float PRICE_KPI_WEIGHT = 0.15f;
    public static float INTERCHANGES_KPI_WEIGHT = 0.15f;
    public static float COMBINED_KPI_WEIGHT = 0.35f;

    // INDIVIDUAL HORIZONS
    public static long TRAVEL_TIME_HORIZON = 60 * 60; // seconds
    public static long PRICE_HORIZON = 3; // eur
    public static long INTERCHANGE_HORIZON = 4; // FM -> PT -> Walk/PT

    //COMBINED HORIZONS
    public static long COMB_TIME_HORIZON = 75 * 60; // seconds
    public static long COMB_PRICE_HORIZON = 5; // eur
    public static long COMB_INTERCHANGE_HORIZON = 5; // f.e. FM -> PT -> Walk -> PT

    public static KPI computeStopKpi(Map<Stop, List<Plan>> allPlans, int numOfAllStops) {

        float avgBestTravelTime = 0;
        float avgAvgTravelTime = 0;
        float avgWorstTravelTime = 0;

        float avgBestPrice = 0;
        float avgAvgPrice = 0;
        float avgWorstPrice = 0;

        float avgBestInterchanges = 0;
        float avgAvgInterchanges = 0;

        //COVERAGE
        float rangedTravelTimeCoverage = 0;
        float rangedPriceCoverage = 0;
        float rangedInterchangesCoverage = 0;
        float combinedRangedCoverage = 0;

        int numOfPlans = allPlans.values().size();

        if (numOfPlans < 1) {
            return new KPI();
        }

        int numOfReachableStops = 0;
        for (List<Plan> plans : allPlans.values()) {

            if (plans == null || plans.isEmpty()) {
                continue;
            }
            numOfReachableStops++;

            long minDuration = Long.MAX_VALUE;
            long maxDuration = Long.MIN_VALUE;
            float innerAvgDuration = 0;

            long minPrice = Long.MAX_VALUE;
            long maxPrice = Long.MIN_VALUE;
            float innerAvgPrice = 0;

            long minLegs = Long.MAX_VALUE;
            float innerAvgLegs = 0;

            boolean containsPlanInCombinedRange = false;
            for (Plan plan : plans) {
                //DURATION
                long duration = plan.getDuration();
                if (duration < minDuration) {
                    minDuration = duration;
                }
                if (duration > maxDuration) {
                    maxDuration = duration;
                }
                innerAvgDuration += duration;

                //PRICE
                long price = plan.getPrice();
                if (price < minPrice) {
                    minPrice = price;
                }
                if (price > maxPrice) {
                    maxPrice = price;
                }
                innerAvgPrice += price;

                //INTERCHANGES
                long numOfLegs = plan.getNumOfLegs();
                if (numOfLegs < minLegs) {
                    minLegs = numOfLegs;
                }
                innerAvgLegs += numOfLegs;

                if (duration <= COMB_TIME_HORIZON && price <= COMB_PRICE_HORIZON && numOfLegs <= COMB_INTERCHANGE_HORIZON) {
                    containsPlanInCombinedRange = true;
                }

            }

//            int numOfPlans = plans.size();
            innerAvgDuration = innerAvgDuration / numOfPlans;
            innerAvgPrice = innerAvgPrice / numOfPlans;
            innerAvgLegs = innerAvgLegs/ numOfPlans;

            // DURATION
            avgBestTravelTime += minDuration;
            avgWorstTravelTime += maxDuration;
            avgAvgTravelTime += innerAvgDuration;

            // PRICE
            avgBestPrice += minPrice;
            avgWorstPrice += maxPrice;
            avgAvgPrice += innerAvgPrice;

            // INTERCHANGES
            avgBestInterchanges += minLegs;
            avgAvgInterchanges += innerAvgLegs;

            // COVERAGE
            rangedTravelTimeCoverage += minDuration <= TRAVEL_TIME_HORIZON ? 1 : 0;
            rangedPriceCoverage += minPrice <= PRICE_HORIZON ? 1 : 0;
            rangedInterchangesCoverage += minLegs <= INTERCHANGE_HORIZON ? 1 : 0;

            combinedRangedCoverage += containsPlanInCombinedRange ? 1 : 0;
        }

        // DURATION
        avgBestTravelTime = avgBestTravelTime / numOfAllStops;
        avgWorstTravelTime = avgWorstTravelTime / numOfAllStops;
        avgAvgTravelTime = avgAvgTravelTime / numOfAllStops;

        // PRICE
        avgBestPrice = avgBestPrice / numOfAllStops;
        avgWorstPrice = avgWorstPrice / numOfAllStops;
        avgAvgPrice = avgAvgPrice / numOfAllStops;

        // INTERCHANGES
        avgBestInterchanges = avgBestInterchanges / numOfAllStops;
        avgAvgInterchanges = avgAvgInterchanges/ numOfAllStops;

        // COVERAGE
        rangedPriceCoverage = rangedPriceCoverage / numOfAllStops;
        rangedTravelTimeCoverage = rangedTravelTimeCoverage / numOfAllStops;
        rangedInterchangesCoverage = rangedInterchangesCoverage / numOfAllStops;
        combinedRangedCoverage = combinedRangedCoverage / numOfAllStops;

        float compundKPI = computeCompoundWeightedKPI(
                rangedTravelTimeCoverage,
                rangedPriceCoverage,
                rangedInterchangesCoverage,
                combinedRangedCoverage
        );

        KPI kpi = new KPI();
        kpi.setAvgBestTravelTime(avgBestTravelTime);
        kpi.setAvgAvgTravelTime(avgAvgTravelTime);
        kpi.setAvgWorstTravelTime(avgWorstTravelTime);

        kpi.setAvgBestPrice(avgBestPrice);
        kpi.setAvgAvgPrice(avgAvgPrice);
        kpi.setAvgWorstPrice(avgWorstPrice);

        kpi.setAvgBestNumOfInterchanges(avgBestInterchanges);
        kpi.setAvgAvgNumOfInterchanges(avgAvgInterchanges);

        kpi.setRangedTravelTimeCoverage(rangedTravelTimeCoverage);
        kpi.setRangedPriceCoverage(rangedPriceCoverage);
        kpi.setRangedInterchangesCoverage(rangedInterchangesCoverage);
        kpi.setCombinedRangedCoverage(combinedRangedCoverage);

        kpi.setCompoundKPI(compundKPI);
        return kpi;
    }

    private static float computeCompoundWeightedKPI(
            float rangedTravelTimeCoverage,
            float rangedPriceCoverage,
            float rangedInterchangesCoverage,
            float combinedRangedCoverage
    ){
        float compoundWeightedKPI = 0;
        compoundWeightedKPI += TRAVEL_TIME_KPI_WEIGHT * rangedTravelTimeCoverage;
        compoundWeightedKPI += PRICE_KPI_WEIGHT * rangedPriceCoverage;
        compoundWeightedKPI += INTERCHANGES_KPI_WEIGHT * rangedInterchangesCoverage;
        compoundWeightedKPI += COMBINED_KPI_WEIGHT * combinedRangedCoverage;

        return compoundWeightedKPI;
    }
}
