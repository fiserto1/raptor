package cz.agents.osrm;

import cz.agents.basestructures.GPSLocation;
import cz.agents.common.PropertyProvider;
import cz.agents.common.TransportMode;
import cz.agents.common.geojson.GeoJSONMapper;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.GPSLocationTools;
import cz.agents.geotools.KDTree;
import cz.agents.geotools.Transformer;
import cz.agents.osrm.services.RouteOsrmService;
import cz.agents.osrm.structures.Profile;
import cz.agents.raptor.MRaptor;
import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.problems.MultiEarliestArrivalProblem;
import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.tools.TimeHelper;
import mil.nga.sf.geojson.FeatureConverter;
import mil.nga.sf.geojson.GeoJsonObject;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    private static final String DEFAULT_DEPARTURE_TIME = "12:30:14";
    //    private static Transformer transformer = new Transformer(25832); // DE SRID
    private static Transformer transformer = new Transformer(27700); // UK SRID
//    private static Transformer transformer = new Transformer(4156); // CZ SRID

    public static void main(String[] args) throws IOException {
//        findOsrmRoute();
        oneToAllWithOsrmAndRaptor();
    }

    private static void oneToAllWithOsrmAndRaptor() throws IOException {

        Gtfs gtfs = loadGTFS();
        Set<TransportMode> allowedModes = loadAllowedModes();
        Timetable timetable = new Timetable(gtfs);
        System.out.println("Timetable Loaded.");

        Map<GPSLocation, List<Stop>> locationStopMap = buildGpsStopMap(timetable);
        System.out.println("Stop map loaded.");


        System.out.println("#stops" + timetable.getStops().values().size());
        KDTree<GPSLocation> kdTree = initKDTree(locationStopMap);
        System.out.println("KDtree filled.");
        Scanner in = new Scanner(System.in);
        String line;

        GtfsDateConverter converter = new GtfsDateConverter();

        while (true) {
            System.out.println("Enter source lat,lon:");
            line = in.nextLine();
            if (line.equalsIgnoreCase("exit")) {
                break;
            }

            String[] split = line.split(",");
            double lat, lon;
            try {
                lat = Double.parseDouble(split[0]);
                lon = Double.parseDouble(split[1]);
            } catch (NumberFormatException e) {
                System.out.println("Wrong number format.");
                continue;
            }

            System.out.println("Enter date (yyyyMMdd):");
            line = in.nextLine();
            LocalDate departureDate;
            try {
                departureDate = converter.convertToEntityAttribute(line);

            } catch (DateTimeParseException e) {
                departureDate = LocalDate.now();
                System.out.println("Wrong date format. Using date: " + departureDate);
            }

            System.out.println("Enter departure time (HH:mm:ss):");
            line = in.nextLine();
            int departureTime;
            try {
                departureTime = TimeHelper.getTimeInSeconds(line);
            } catch (NumberFormatException e) {
                System.out.println("Wrong number format. Using default: " + DEFAULT_DEPARTURE_TIME);
                departureTime = TimeHelper.getTimeInSeconds(DEFAULT_DEPARTURE_TIME);
            }

            int departureDateTime = TimeHelper.secondsBetween(timetable.getStartDate(), departureDate) + departureTime;
            GPSLocation sourceLocation = GPSLocationTools.createGPSLocation(lat, lon,0, transformer);

            MultiEarliestArrivalProblem eap = new MultiEarliestArrivalProblem(timetable, sourceLocation, null, departureDateTime, allowedModes);

            long startTime = System.currentTimeMillis();
            FirstMileSolver firstMileSolver = new FirstMileSolver();
            firstMileSolver.solve(eap, kdTree, locationStopMap);
            System.out.println("FM Time:" + (System.currentTimeMillis() - startTime));

            startTime = System.currentTimeMillis();
            MRaptor raptor = new MRaptor();
            List<Plan> plans = raptor.solve(eap);
            System.out.println("Raptor Time: " + (System.currentTimeMillis() - startTime));
            System.out.println("# of plans: " + plans.size());

//            System.out.println("Departure Date Time: " + departureDateTime);
//            GeoJsonObject geoJsonObject = GeoJSONMapper.parseStopsToGeoJson(eap.getStopData(), departureDateTime);
            GeoJsonObject geoJsonObject = GeoJSONMapper.parsePlansToGeoJson(plans);
            String geojsonStringValue = FeatureConverter.toStringValue(geoJsonObject);
            String timestamp = departureDate.atTime(LocalTime.ofSecondOfDay(departureTime)).format(
                    DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"));
            try (FileWriter wr = new FileWriter(new File("raptor-output-man-" + timestamp + ".geojson"))) {
                wr.write(geojsonStringValue);
            } catch (IOException e) {
                e.printStackTrace();
            }


//            for (Stop stop : timetable.getStops().values()) {
//                int earliestArrival = eap.getEarliestArrival(stop);
//                System.out.println("Stop: " + stop.getName() + " eat:" + earliestArrival + " that's " + (earliestArrival - departureDateTime) + " seconds" );
//            }
        }
    }

    private static Set<TransportMode> loadAllowedModes() {
        Set<TransportMode> allowedModes = new HashSet<>();

        PropertyProvider propertyProvider = PropertyProvider.getInstance();
        String modes = propertyProvider.getProperty("raptor.modes");
        String[] split = modes.split(",");
        for (String s : split) {
            try {
                TransportMode transportMode = TransportMode.valueOf(s.toUpperCase());
                allowedModes.add(transportMode);
            } catch (IllegalArgumentException e) {
                System.out.println("Transport mode " + s + " does not exists.");
            }
        }
        return allowedModes;
    }

    private static void findOsrmRoute() {
        Scanner in = new Scanner(System.in);
        String line;

        while (true) {
            System.out.println("Enter source lat,lon:");
            line = in.nextLine();
            String startCoord = line;

            System.out.println("Enter target lat,lon:");
            line = in.nextLine();
            String targetCoord = line;

            RouteOsrmService routeOsrmService = new RouteOsrmService();
            String coord = String.format("%s;%s", startCoord,targetCoord);
            JSONObject fastestRoute = routeOsrmService.getFastestRoute(coord, Profile.DRIVING.name().toLowerCase());

            System.out.println("Journey:");
            System.out.println(fastestRoute.toString());
        }
    }

    public static KDTree<GPSLocation> initKDTree(Map<GPSLocation, List<Stop>> locationGtfsStopsMap) {
        KDTree<GPSLocation> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),
                KDTree.ConflictResolverMode.USE_OLD, GPSLocation[]::new); //TODO array constructor?
        for (GPSLocation gpsLocation : locationGtfsStopsMap.keySet()) {
            kdTree.insert(gpsLocation);
        }
        return kdTree;
    }

    public static Map<GPSLocation, List<Stop>> buildGpsStopMap(Timetable timetable) {
        Map<GPSLocation, List<Stop>> locationStopMap = new HashMap<>();
        for (Map.Entry<String, Stop> stopEntry : timetable.getStops().entrySet()) {
            Stop stop = stopEntry.getValue();
            GPSLocation gpsLocation = GPSLocationTools.createGPSLocation(stop.getStopLat(), stop.getStopLon(), 0, transformer);
            if (locationStopMap.containsKey(gpsLocation)) {
                locationStopMap.get(gpsLocation).add(stop);
            } else {
                List<Stop> stops = new ArrayList<>();
                stops.add(stop);
                locationStopMap.put(gpsLocation, stops);
            }
        }
        return locationStopMap;
    }

    private static Gtfs loadGTFS() {
        PropertyProvider propertyProvider = PropertyProvider.getInstance();

        String gtfsFilepath = propertyProvider.getProperty("gtfs.filepath");

        Gtfs gtfs = null;
        try {
            System.out.println("Loading...");
            gtfs = Gtfs.parseFolder(gtfsFilepath); // TEST_TIMETABLE can be used
        } catch (ObjectImportException e) {
            e.printStackTrace();
        }
        gtfs.removeIllegalElements();
        System.out.println(gtfs);
        return gtfs;
    }
}
