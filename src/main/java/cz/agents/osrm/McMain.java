package cz.agents.osrm;

import cz.agents.basestructures.BoundingBox;
import cz.agents.basestructures.GPSLocation;
import cz.agents.common.PropertyProvider;
import cz.agents.common.TransportMode;
import cz.agents.common.geojson.GeoJSONMapper;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.GPSLocationTools;
import cz.agents.geotools.KDTree;
import cz.agents.geotools.Transformer;
import cz.agents.osrm.services.RouteOsrmService;
import cz.agents.osrm.structures.Profile;
import cz.agents.raptor.McRaptor;
import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.problems.McProblem;
import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.tools.TimeHelper;
import mil.nga.sf.Point;
import mil.nga.sf.geojson.Feature;
import mil.nga.sf.geojson.FeatureCollection;
import mil.nga.sf.geojson.FeatureConverter;
import mil.nga.sf.geojson.GeoJsonObject;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class McMain {
    public static final boolean SHOULD_LOG = false;

    private static final String DEFAULT_DEPARTURE_TIME = "12:30:14";

    private static final boolean SHOULD_CREATE_GEOJSON_STOPS = false;
    private static final boolean SHOULD_PICK_FEWER_LOCATIONS = false;

    private static Transformer transformer = new Transformer(25832); // DE SRID
//    private static Transformer transformer = new Transformer(27700); // UK SRID
//    private static Transformer transformer = new Transformer(4156); // CZ SRID

    public static void main(String[] args) throws IOException {
        BoundingBox boundingBox = new BoundingBox(12987917, 52288492, 13855836, 52721822);
        BoundingBox centerBoundingBox = new BoundingBox(13308978, 52478721, 13481875, 52561448);

//        findOsrmRoute();
//        oneToAllWithOsrmAndRaptor(boundingBox);
        String filenamePrefix = "all-to-all-berlin-car-walk-bike-";
        allToAll(boundingBox, centerBoundingBox, filenamePrefix);
    }

    private static void allToAll(BoundingBox boundingBox, BoundingBox centerBoundingBox, String filenamePrefix) throws IOException {

        PropertyProvider propertyProvider = PropertyProvider.getInstance();

        String gridFilepath = propertyProvider.getProperty("grid.inner.filepath");
        List<GPSLocation> gridLocations = loadGrid(gridFilepath);
        gridLocations.removeIf(l -> !centerBoundingBox.inside(l.getLonE6(), l.getLatE6()));

        gridFilepath = propertyProvider.getProperty("grid.outer.filepath");
        List<GPSLocation> outerLocations = loadGrid(gridFilepath);
        outerLocations.removeIf(l -> centerBoundingBox.inside(l.getLonE6(), l.getLatE6()));

        gridLocations.addAll(outerLocations);
        System.out.println("# locations in grid: " + gridLocations.size());

        GeoJsonObject gridGeoJson = GeoJSONMapper.parseGPSLocationsToGeoJson(gridLocations);
        GeoJSONMapper.writeGeoJson("combined-grid-", LocalDateTime.now(), gridGeoJson);


        if (SHOULD_PICK_FEWER_LOCATIONS) {
            Collections.shuffle(gridLocations);
            gridLocations = gridLocations.subList(0, 50);
        }

        //SAME
        Gtfs gtfs = loadGTFS(boundingBox);
        Set<TransportMode> allowedModes = loadAllowedModes();
        Timetable timetable = new Timetable(gtfs);
        System.out.println("Timetable Loaded.");

        Map<GPSLocation, List<Stop>> locationStopMap = buildGpsStopMap(timetable);
        System.out.println("Stop map loaded.");

        System.out.println("#stops" + timetable.getStops().values().size());
        KDTree<GPSLocation> kdTree = initKDTree(locationStopMap);
        System.out.println("KDtree filled.");
        // END

        GtfsDateConverter converter = new GtfsDateConverter();

        String dateProperty = PropertyProvider.getInstance().getProperty("analyser.departure.date");
        LocalDate departureDate;
        try {
            departureDate = converter.convertToEntityAttribute(dateProperty);
        } catch (DateTimeParseException e) {
            departureDate = LocalDate.now();
            System.out.println("Wrong date format. Using date: " + departureDate);
        }


        String time = PropertyProvider.getInstance().getProperty("analyser.departure.time");
        int departureTime;
        try {
            departureTime = TimeHelper.getTimeInSeconds(time);
        } catch (NumberFormatException e) {
            System.out.println("Wrong number format. Using default: " + DEFAULT_DEPARTURE_TIME);
            departureTime = TimeHelper.getTimeInSeconds(DEFAULT_DEPARTURE_TIME);
        }

        int departureDateTime = TimeHelper.secondsBetween(timetable.getStartDate(), departureDate) + departureTime;

        long programStartTime = System.currentTimeMillis();

        int numOfAllStops = timetable.getStops().size();
        Map<GPSLocation, KPI> allKPIs = new HashMap<>();
        for (int i = 0; i < gridLocations.size(); i++) {
            GPSLocation sourceLocation = gridLocations.get(i);
            System.out.println(String.format(
                    "====================================================== \n" +
                            "Planning from %d th location.",
                    i
            ));

            McProblem problem = new McProblem(timetable, sourceLocation, null, departureDateTime, allowedModes);

            //OSRM
            long startTime = System.currentTimeMillis();
            FirstMileSolver firstMileSolver = new FirstMileSolver();
            firstMileSolver.solve(problem, kdTree, locationStopMap);
            System.out.println("FM Time:" + (System.currentTimeMillis() - startTime));

            //McRAPTOR
            startTime = System.currentTimeMillis();
            McRaptor raptor = new McRaptor();
            Map<Stop, List<Plan>> plans = raptor.solve(problem);
            System.out.println("Raptor Time: " + (System.currentTimeMillis() - startTime));
            System.out.println("# of plans: " + plans.size());

            //Compute KPIs
            KPI locationKPI = KpiUtils.computeStopKpi(plans, numOfAllStops);
            System.out.println(locationKPI);
            allKPIs.put(sourceLocation, locationKPI);

        }

        System.out.println("Program time: " + (System.currentTimeMillis() - programStartTime));

        LocalDateTime departureTimestamp = departureDate.atTime(LocalTime.ofSecondOfDay(departureTime));
        GeoJsonObject geoJsonObject = GeoJSONMapper.parseLocationsWithKPIToGeoJson(allKPIs);
        GeoJSONMapper.writeGeoJson(filenamePrefix, departureTimestamp, geoJsonObject);
    }

    private static List<GPSLocation> loadGrid(String gridFilepath) throws IOException {

        String content = new String(Files.readAllBytes(Paths.get(gridFilepath)), StandardCharsets.UTF_8);
        FeatureCollection features = FeatureConverter.toFeatureCollection(content);
        List<Feature> featureList = features.getFeatures();

        List<GPSLocation> locations = new ArrayList<>();
        for (Feature feature : featureList) {
            Point centroid = feature.getFeature().getGeometry().getCentroid();
            double lat = centroid.getY();
            double lon = centroid.getX();
            GPSLocation location = GPSLocationTools.createGPSLocation(lat, lon,0, transformer);
            locations.add(location);
        }
        return locations;
    }

    private static void oneToAllWithOsrmAndRaptor(BoundingBox boundingBox) throws IOException {

        Gtfs gtfs = loadGTFS(boundingBox);
        Set<TransportMode> allowedModes = loadAllowedModes();
        Timetable timetable = new Timetable(gtfs);
        System.out.println("Timetable Loaded.");

        Map<GPSLocation, List<Stop>> locationStopMap = buildGpsStopMap(timetable);
        System.out.println("Stop map loaded.");
        if (SHOULD_CREATE_GEOJSON_STOPS) {
            GeoJsonObject allStopsGeoJson = GeoJSONMapper.parseStopsToGeoJson(timetable.getStops());
            GeoJSONMapper.writeGeoJson("stops-berlin", LocalDateTime.now(), allStopsGeoJson);
        }


        System.out.println("#stops" + timetable.getStops().values().size());
        KDTree<GPSLocation> kdTree = initKDTree(locationStopMap);
        System.out.println("KDtree filled.");

        GtfsDateConverter converter = new GtfsDateConverter();
        Scanner in = new Scanner(System.in);
        String line;

        int numOfAllStops = timetable.getStops().size();

        while (true) {
            System.out.println("Enter source lat,lon:");
            line = in.nextLine();
            if (line.equalsIgnoreCase("exit")) {
                break;
            }

            String[] split = line.split(",");
            double lat, lon;
            try {
                lat = Double.parseDouble(split[0]);
                lon = Double.parseDouble(split[1]);
            } catch (NumberFormatException e) {
                System.out.println("Wrong number format.");
                continue;
            }

            System.out.println("Enter date (yyyyMMdd):");
            line = in.nextLine();
            LocalDate departureDate;
            try {
                departureDate = converter.convertToEntityAttribute(line);

            } catch (DateTimeParseException e) {
                departureDate = LocalDate.now();
                System.out.println("Wrong date format. Using date: " + departureDate);
            }

            System.out.println("Enter departure time (HH:mm:ss):");
            line = in.nextLine();
            int departureTime;
            try {
                departureTime = TimeHelper.getTimeInSeconds(line);
            } catch (NumberFormatException e) {
                System.out.println("Wrong number format. Using default: " + DEFAULT_DEPARTURE_TIME);
                departureTime = TimeHelper.getTimeInSeconds(DEFAULT_DEPARTURE_TIME);
            }

            int departureDateTime = TimeHelper.secondsBetween(timetable.getStartDate(), departureDate) + departureTime;
            GPSLocation sourceLocation = GPSLocationTools.createGPSLocation(lat, lon,0, transformer);

            McProblem problem = new McProblem(timetable, sourceLocation, null, departureDateTime, allowedModes);

            long startTime = System.currentTimeMillis();
            FirstMileSolver firstMileSolver = new FirstMileSolver();
            firstMileSolver.solve(problem, kdTree, locationStopMap);
            System.out.println("FM Time:" + (System.currentTimeMillis() - startTime));

            startTime = System.currentTimeMillis();
            McRaptor raptor = new McRaptor();
            Map<Stop, List<Plan>> plans = raptor.solve(problem);
            System.out.println("Raptor Time: " + (System.currentTimeMillis() - startTime));
            System.out.println("# of plans: " + plans.size());

            KPI stopKpi = KpiUtils.computeStopKpi(plans, numOfAllStops);
            System.out.println(stopKpi);

//            System.out.println("Departure Date Time: " + departureDateTime);
//            GeoJsonObject geoJsonObject = GeoJSONMapper.parseStopsToGeoJson(problem.getStopData(), departureDateTime);
            GeoJsonObject geoJsonObject = GeoJSONMapper.parseParetoPlansToGeoJson(plans);
            String geojsonStringValue = FeatureConverter.toStringValue(geoJsonObject);
            String timestamp = departureDate.atTime(LocalTime.ofSecondOfDay(departureTime)).format(
                    DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"));
            try (FileWriter wr = new FileWriter(new File("raptor-output-man-" + timestamp + ".geojson"))) {
                wr.write(geojsonStringValue);
            } catch (IOException e) {
                e.printStackTrace();
            }


//            for (Stop stop : timetable.getStops().values()) {
//                int earliestArrival = problem.getEarliestArrival(stop);
//                System.out.println("Stop: " + stop.getName() + " eat:" + earliestArrival + " that's " + (earliestArrival - departureDateTime) + " seconds" );
//            }
        }
    }

    private static Set<TransportMode> loadAllowedModes() {
        Set<TransportMode> allowedModes = new HashSet<>();

        PropertyProvider propertyProvider = PropertyProvider.getInstance();
        String modes = propertyProvider.getProperty("raptor.modes");
        String[] split = modes.split(",");
        for (String s : split) {
            try {
                TransportMode transportMode = TransportMode.valueOf(s.toUpperCase());
                allowedModes.add(transportMode);
            } catch (IllegalArgumentException e) {
                System.out.println("Transport mode " + s + " does not exists.");
            }
        }
        return allowedModes;
    }

    private static void findOsrmRoute() {
        Scanner in = new Scanner(System.in);
        String line;

        while (true) {
            System.out.println("Enter source lat,lon:");
            line = in.nextLine();
            String startCoord = line;

            System.out.println("Enter target lat,lon:");
            line = in.nextLine();
            String targetCoord = line;

            RouteOsrmService routeOsrmService = new RouteOsrmService();
            String coord = String.format("%s;%s", startCoord,targetCoord);
            JSONObject fastestRoute = routeOsrmService.getFastestRoute(coord, Profile.DRIVING.name().toLowerCase());

            System.out.println("Journey:");
            System.out.println(fastestRoute.toString());
        }
    }

    public static KDTree<GPSLocation> initKDTree(Map<GPSLocation, List<Stop>> locationGtfsStopsMap) {
        KDTree<GPSLocation> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),
                KDTree.ConflictResolverMode.USE_OLD, GPSLocation[]::new);
        for (GPSLocation gpsLocation : locationGtfsStopsMap.keySet()) {
            kdTree.insert(gpsLocation);
        }
        return kdTree;
    }

    /**
     * Maps the stops by their locations.
     *
     * @implNote There could be more stops in a one location!
     *
     * @param timetable a timetable
     * @return map with GPS locations and stops assigned to them
     */
    public static Map<GPSLocation, List<Stop>> buildGpsStopMap(Timetable timetable) {
        Map<GPSLocation, List<Stop>> locationStopMap = new HashMap<>();
        for (Map.Entry<String, Stop> stopEntry : timetable.getStops().entrySet()) {
            Stop stop = stopEntry.getValue();
            GPSLocation gpsLocation = GPSLocationTools.createGPSLocation(stop.getStopLat(), stop.getStopLon(), 0, transformer);
            if (locationStopMap.containsKey(gpsLocation)) {
                locationStopMap.get(gpsLocation).add(stop);
            } else {
                List<Stop> stops = new ArrayList<>();
                stops.add(stop);
                locationStopMap.put(gpsLocation, stops);
            }
        }
        return locationStopMap;
    }

    private static Gtfs loadGTFS() {
        return loadGTFS(null);
    }

    private static Gtfs loadGTFS(BoundingBox bbox) {
        PropertyProvider propertyProvider = PropertyProvider.getInstance();

        String gtfsFilepath = propertyProvider.getProperty("gtfs.filepath");

        Gtfs gtfs = null;
        try {
            System.out.println("Loading...");
            gtfs = Gtfs.parseFolder(gtfsFilepath); // TEST_TIMETABLE can be used
        } catch (ObjectImportException e) {
            e.printStackTrace();
        }

        if (gtfs == null) {
            return null;
        }

        if (bbox != null) {
            gtfs.removeStops(s -> {
                GPSLocation gpsLocation = GPSLocationTools.createGPSLocation(s.stopLat, s.stopLon, 0, transformer);
                return !bbox.inside(gpsLocation.getLonE6(), gpsLocation.getLatE6());
            });
        }

        gtfs.removeIllegalElements();
        System.out.println(gtfs);
        return gtfs;
    }
}
