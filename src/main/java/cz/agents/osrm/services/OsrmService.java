package cz.agents.osrm.services;

import cz.agents.common.PropertyProvider;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.net.URI;

public abstract class OsrmService {
    protected final PropertyProvider propertyProvider;

    public OsrmService(){
        this.propertyProvider = PropertyProvider.getInstance();
    }

    protected JSONObject callApi(String url) {
        HttpClient httpClient = HttpClients.createDefault();
        JSONObject result = null;
        try {
            URIBuilder builder = new URIBuilder(url);
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.addHeader("accept", "application/json");
            HttpResponse response = httpClient.execute(request);
            result = new JSONObject(IOUtils.toString(response.getEntity().getContent()));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

}
