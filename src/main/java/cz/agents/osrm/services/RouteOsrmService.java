package cz.agents.osrm.services;

import org.json.JSONObject;

/**
 * Routing and direction tasks using the Open Source Routing Machine HTTP API.
 * @author An Tran
 * @see <a href="http://project-osrm.org/docs/v5.7.0/api/#route-service">http://project-osrm.org</a>
 *
 */
public class RouteOsrmService extends OsrmService {

	public RouteOsrmService() {
	}

	/**
	 * Finds the fastest route between coordinates in the supplied order, accounting for mode of travel.
	 * @param coordinates The string containing comma separated lon/lat. Multiple coordinate pairs are separated by a semicolon.
	 * @param profile The string specifying mode of travel. Valid values are 'car', 'bike' and 'foot'.
	 * @return A JSON object containing the response code, an array of waypoint objects, and an array of route objects.
	 */
	public JSONObject getFastestRoute(String coordinates, String profile) {
		return callApi(buildUrl(coordinates, profile));
	}

	protected String buildUrl(String coordinates, String profile) {
		String baseUrl = propertyProvider.getProperty("osrm.route." + profile + ".api.url");
		return String.format(baseUrl + "%s/%s?geometries=geojson&overview=full", profile, coordinates);
	}

}
