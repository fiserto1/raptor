package cz.agents.osrm.services;

import cz.agents.osrm.McMain;
import org.json.JSONObject;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Calculates the travel time matrix of the fastest route between all supplied coordinates.
 * No geographic data is returned with this service.
 * @author An Tran
 * @see http://project-osrm.org/docs/v5.7.0/api/#table-service
 * 
 */
public class TableOsrmService extends OsrmService {


	/**
	 * Finds the fastest route between coordinates in the supplied order
	 * accounting for mode of travel.
	 * @param coordinates The string containing comma separated lon/lat. Multiple coordinate pairs are separated by a semicolon.
	 * @param profile The mode of travel. Valid values are 'car', 'bike' and 'foot'.
	 * @return A JSON object containing the response code, a 2d array of durations, and arrays of waypoint objects representing sources, and an destinations.
	 */
	public JSONObject generateTravelTimeMatrix(String coordinates, String profile, List<Integer> sources, List<Integer> destinations) {
		String url = buildUrl(coordinates, profile, sources, destinations);
		if (McMain.SHOULD_LOG) {
			System.out.println("Calling the api with URL: " + url);
		}
		return callApi(url);
	}

	private String buildUrl(String coordinates, String profile, List<Integer> sources, List<Integer> destinations) {
		//example of sources/destination value= 1;2;3
		String sourcesString = String.join(";", sources.stream().map(Object::toString).collect(Collectors.toList()));
		String destinationsString = String.join(";", destinations.stream().map(Object::toString).collect(Collectors.toList()));

		String baseUrl = propertyProvider.getProperty("osrm.table." + profile + ".api.url");
		return String.format(baseUrl + "%s/%s?sources=%s&destinations=%s",
				profile, coordinates, sourcesString, destinationsString);
	}

}
