package cz.agents.osrm.structures;

public enum Profile {
    DRIVING, WALKING, CYCLING
}
