package cz.agents.raptor;

import cz.agents.common.TransportMode;
import cz.agents.raptor.structures.Stop;

public class Leg {
    private final Stop sourceStop;
    private final Stop targetStop;
    private final int duration;
    private final TransportMode mode;

    public Leg(Stop sourceStop, Stop targetStop, int duration, TransportMode mode) {
        this.sourceStop = sourceStop;
        this.targetStop = targetStop;
        this.duration = duration;
        this.mode = mode;
    }

    public Stop getSourceStop() {
        return sourceStop;
    }

    public Stop getTargetStop() {
        return targetStop;
    }

    public int getDuration() {
        return duration;
    }

    public TransportMode getMode() {
        return mode;
    }
}
