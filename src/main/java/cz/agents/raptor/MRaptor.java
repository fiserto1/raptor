package cz.agents.raptor;

import cz.agents.common.TransportMode;
import cz.agents.raptor.problems.MultiEarliestArrivalProblem;
import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.MultiTransfer;
import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Route;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MRaptor {
    public static final int MAX_TRIPS = 5; // maximal number of trips in the journey
    protected int round;                      // current number of trips
    protected MultiEarliestArrivalProblem eap;
    protected Set<Stop> markedStops;
    protected HashMap<Route, Integer> activeRoutes;

    /**
     * Takes an instance of the earliest arrival problem as an input and returns the journey solving this problem.
     *
     * @param eap an instance of the earliest arrival problem
     * @return a journey which solves eap, null if no journey is found
     */
    public List<Plan> solve(MultiEarliestArrivalProblem eap) {
        this.eap = eap;
        markedStops = new HashSet<>();
        activeRoutes = new HashMap<>();
        round = 0;

        markDepartureStopsByTransfers(eap.getFirstMileTransfers());

        round++;

        while (!markedStops.isEmpty() && round <= MAX_TRIPS) {
            collectActiveRoutes();
            processActiveRoutes();
            processTransfers();

            round++;
        }

        return reconstructPlans();
    }

    /**
     * Retrieves all neighbours of a stop (reachable by a transfer) and initializes them as the departure stops.
     *
     */
    private void markDepartureStopsByTransfers(List<Transfer> initialTransfers) {
        int departureDateTime = eap.getDepartureDateTime();
        for (Transfer transfer : initialTransfers) {
            updateArrivalByTransfer(transfer, transfer.getTargetStop(), departureDateTime + transfer.getTransferTime());
        }
    }

    /**
     * Sets the index of the earliest stop from which an active route should be processed.
     * If a higher index is already set for the route, it gets updated.
     *
     * @param stopRoute a route which should be active in the current round
     * @param stopIndex an index of a marked stop on the route
     */
    private void setEarliestStopIndex(Route stopRoute, int stopIndex) {
        if (activeRoutes.containsKey(stopRoute)) {
            int existingStopIndex = activeRoutes.get(stopRoute);

            if (stopIndex < existingStopIndex) {
                activeRoutes.put(stopRoute, stopIndex);
            }
        } else {
            activeRoutes.put(stopRoute, stopIndex);
        }
    }

    /**
     * Collects all active routes for the current round. An active route is a route with an active stop.
     * Notes an index of the earliest marked stop on the route.
     */
    protected void collectActiveRoutes() {
        activeRoutes.clear();

        Iterator<Stop> markedStopsIt = markedStops.iterator();

        while (markedStopsIt.hasNext()) {
            Stop stop = markedStopsIt.next();

            List<Route> stopRoutes = stop.getRoutes();
            List<Integer> stopRoutesIndices = stop.getIndices();

            for (int i = 0; i < stopRoutes.size(); i++) {
                Route route = stopRoutes.get(i);

                int stopIndex = stopRoutesIndices.get( i);
                setEarliestStopIndex(route, stopIndex);
            }
            markedStopsIt.remove();
        }
    }

    /**
     * Finds an earliest trip that departs from a stop after the given arrival time.
     *
     * @param route a route containing the stop
     * @param previousArrival an arrival time
     * @param stopIndex an index of the stop on the route
     * @return an earliest trip departing after the given arrival time, null if trip is not found
     */
    private Trip findEarliestTrip(Route route, int previousArrival, int stopIndex) {
        List<Trip> routeTrips = route.getTrips();
        int left = 0;
        int right = routeTrips.size() - 1;
        int middle = 0;
        int departure = 0;

        if (routeTrips.size() == 0) {
//            System.err.println("Warning: Route " + route.getRouteId() + " (" + route.getId() + ") contains no trips.");
            return null;
        }
        while (left <= right) {
            middle = (left + right) / 2;
            departure = routeTrips.get(middle).getDepartureAtStop(stopIndex);

            if (departure == previousArrival) {
                return routeTrips.get(middle);
            } else if (departure < previousArrival) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        if (middle == routeTrips.size() - 1) {    // out of bounds
            return null;
        } else if (departure < previousArrival) { // next earliest departure is the next one
            return routeTrips.get(middle+1);
        } else {                                  // next earliest departure is the current one
            return routeTrips.get(middle);
        }
    }

    /**
     * Processes transfers of marked stops. Checks if any of the transfers can be used in the journey.
     */
    protected void processTransfers() {
        Set<Stop> markedStopsCopy = new HashSet<>(markedStops);

        for (Stop stop : markedStopsCopy) {
            if (eap.getTransfer(stop, round) != null) {
                continue;
            }
            for (Transfer transfer : stop.getTransfers()) {
                Stop transferTargetStop = transfer.getTargetStop();

                int newArrival = eap.getArrivalTime(stop, round) + transfer.getTransferTime();
                updateArrivalByTransfer(transfer, transferTargetStop, newArrival);
            }
        }
    }

    /**
     * Processes trips at active routes. Checks if any of the trips can be used in the journey.
     */
    protected void processActiveRoutes() {
        for (Map.Entry<Route, Integer> activeRoute : activeRoutes.entrySet()) {
            Trip currentTrip = null;
            Stop pickUpStop = null;
            Route route = activeRoute.getKey();
            List<Stop> routeStops = route.getStops();

            // process the route from the earliest marked stop
            for (int stopIndex = activeRoute.getValue(); stopIndex < routeStops.size(); stopIndex++) {
                Stop stop = routeStops.get(stopIndex);

                if (currentTrip != null) {
                    int newArrival = currentTrip.getArrivalAtStop(stopIndex);
                    updateArrivalByTrip(currentTrip, stop, pickUpStop, newArrival);
                }
                int previousArrival = eap.getArrivalTime(stop, round - 1);

                // possibility to find a new / earlier trip
                if (currentTrip == null || previousArrival < currentTrip.getDepartureAtStop(stopIndex)) {
                    Trip earliestTrip = findEarliestTrip(route, previousArrival, stopIndex);

                    // there is an earlier trip that can be used
                    if (earliestTrip != null && earliestTrip != currentTrip) {
                        currentTrip = earliestTrip;
                        pickUpStop = stop;
                    }
                }
            }
        }
    }

    protected void updateArrivalByTransfer(Transfer transfer, Stop stop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop)) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTransfer(stop, round, transfer);
            markedStops.add(stop);
        }
    }

    protected void updateArrivalByTrip(Trip trip, Stop stop, Stop pickUpStop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop)) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTrip(stop, round, trip);
            eap.setPreviousStop(stop, round, pickUpStop);
            markedStops.add(stop);
        }
    }

    /**
     * Finds out how many trips it took to get to the target stop.
     *
     * @return a number of trips that solves the earliest arrival problem
     */
    protected int getJourneyTripCount() {
        return -1;
    }

    /**
     * Collects all the trips and stops that make up the result journey.
     *
     * @return a result journey (a collection of trips and transfers to use for solving the EAP)
     */
    protected List<Plan> reconstructPlans() {
        List<Plan> plans = new ArrayList<>();

        List<Stop> targetStops = eap.getTargetStops();
        if (targetStops == null) {
            targetStops = new ArrayList<>(eap.getStopData().keySet());
        }

        for (Stop targetStop : targetStops) {
            MultiEarliestArrivalProblem.StopData stopData = eap.getStopData(targetStop);
            int tripCount = stopData.getTripCount();

            if (tripCount == -1) {
//                plans.add(null);  // TODO: 04-Dec-18 collect also the unreachable stops?
                continue;
            }

            long duration = 0;
            int transferCount = 0;
            LinkedList<Leg> legs = new LinkedList<>();
            Stop currentStop = targetStop;
            for (int i = tripCount; i > 0; i--) {
                Transfer transfer = eap.getTransfer(currentStop, i);

                if (transfer != null) {
                    duration += transfer.getTransferTime();
                    legs.addFirst(new Leg(transfer.getSourceStop(), transfer.getTargetStop(), transfer.getTransferTime(), TransportMode.FOOT));
                    currentStop = transfer.getSourceStop();
                    transferCount++;
                }

                Trip trip = eap.getTrip(currentStop, i);
                Stop pickUpStop = eap.getPreviousStop(currentStop, i);

                int tripDuration = trip.getArrivalAtStop(currentStop) - trip.getDepartureAtStop(pickUpStop);
                duration += tripDuration;
                legs.addFirst(new Leg(pickUpStop, currentStop, tripDuration, TransportMode.PT));
                currentStop = pickUpStop;

            }
            Transfer transfer = eap.getTransfer(currentStop, 0);
            MultiEarliestArrivalProblem.StopData currentStopData = eap.getStopData(currentStop);
            if (currentStopData.earliestArrivalTimeList[0] == currentStopData.earliestArrivalTime) {
                if (transfer != null) {
                    TransportMode mode;
                    if (transfer instanceof MultiTransfer) {
                        mode = ((MultiTransfer) transfer).getModes().get(0);
                    } else {
                        mode = TransportMode.FOOT;
                    }
                    legs.addFirst(new Leg(transfer.getSourceStop(), transfer.getTargetStop(), transfer.getTransferTime(), mode));
                    duration += transfer.getTransferTime();
                }
            }

            Plan plan = new Plan(targetStop, duration, 0, tripCount, transferCount, tripCount+transferCount, legs);
            plans.add(plan);
        }

        return plans;
    }
}
