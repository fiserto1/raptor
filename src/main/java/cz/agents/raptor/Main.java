package cz.agents.raptor;

import cz.agents.common.PropertyProvider;
import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.problems.EarliestArrivalProblem;
import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.tools.TimeHelper;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        Scanner in = new Scanner(System.in);
        String line;

        PropertyProvider propertyProvider = PropertyProvider.getInstance();

        String gtfsFilepath = propertyProvider.getProperty("gtfs.filepath");

        Gtfs gtfs = null;
        try {
            System.out.println("Loading...");
            gtfs = Gtfs.parseFolder(gtfsFilepath); // TEST_TIMETABLE can be used
        } catch (ObjectImportException e) {
            e.printStackTrace();
        }
        gtfs.removeIllegalElements();
        System.out.println(gtfs);

        Timetable timetable = new Timetable(gtfs);

        while (true) {
            System.out.println("Enter source stop id:");
            line = in.nextLine();
            Stop sourceStop = timetable.getStopById(line);

            if (sourceStop == null) {
                System.out.println("Source stop not found.");
                continue;
            }

            System.out.println("Enter target stop id:");
            line = in.nextLine();
            Stop targetStop = timetable.getStopById(line);

            if (targetStop == null) {
                System.out.println("Target stop not found.");
                continue;
            }

            System.out.println("Enter date (yyyyMMdd):");
            line = in.nextLine();

            GtfsDateConverter converter = new GtfsDateConverter();
            LocalDate date = converter.convertToEntityAttribute(line);


            System.out.println("Enter departure time (HH:mm:ss):");
            line = in.nextLine();
            int departureTime = TimeHelper.getTimeInSeconds(line);

            EarliestArrivalProblem eap = new EarliestArrivalProblem(timetable, sourceStop, targetStop, date, departureTime);

            Raptor raptor = new Raptor();
            Journey journey = raptor.solve(eap);

            if (journey != null) {
                journey.print();
            } else {
                System.out.println("Journey not found.");
            }
        }
    }
}
