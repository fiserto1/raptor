package cz.agents.raptor;

import cz.agents.raptor.problems.McProblem;
import cz.agents.raptor.structures.Label;
import cz.agents.raptor.structures.MultiTransfer;
import cz.agents.raptor.structures.Plan;
import cz.agents.raptor.structures.Route;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.DominationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class McRaptor {
    public static final int MAX_ROUNDS = 5; // maximal number of trips in the journey
    private static final int MAX_FOLLOWING_TRIPS = 0; // if 0, the earliest trip is selected only
    private static final int SINGLE_TICKET_PRICE = 3; //EUR
    private static final boolean SHOULD_COMPUTE_LEGS = true;
    protected int round;                      // current number of trips
    protected McProblem eap;
    protected Set<Stop> markedStops;
    protected HashMap<Route, Integer> activeRoutes;

    /**
     * Takes an instance of the earliest arrival problem as an input and returns the journey solving this problem.
     *
     * @param eap an instance of the earliest arrival problem
     * @return a journey which solves eap, null if no journey is found
     */
    public Map<Stop, List<Plan>> solve(McProblem eap) {
        this.eap = eap;
        markedStops = new HashSet<>();
        activeRoutes = new HashMap<>();
        round = 0;

        markDepartureStopsByTransfers(eap.getFirstMileTransfers());

        round++;

        while (!markedStops.isEmpty() && round <= MAX_ROUNDS) {
            System.out.println("Round: " + round + " started.");
            collectActiveRoutes();
            processActiveRoutes();
            processTransfers();

            round++;
        }

        return reconstructPlans();
    }

    /**
     * Retrieves all neighbours of a stop (reachable by a transfer) and initializes them as the departure stops.
     *
     */
    private void markDepartureStopsByTransfers(List<Transfer> initialTransfers) {
        int departureDateTime = eap.getDepartureDateTime();
        for (Transfer transfer : initialTransfers) {
            int arrivalTime = departureDateTime + transfer.getTransferTime();
            int price = computeTransferPrice(transfer);
            updateArrivalByTransfer(transfer, transfer.getTargetStop(), new int[]{arrivalTime, price}, null);
        }
    }

    /**
     * Sets the index of the earliest stop from which an active route should be processed.
     * If a higher index is already set for the route, it gets updated.
     *
     * @param stopRoute a route which should be active in the current round
     * @param stopIndex an index of a marked stop on the route
     */
    private void setEarliestStopIndex(Route stopRoute, int stopIndex) {
        if (activeRoutes.containsKey(stopRoute)) {
            int existingStopIndex = activeRoutes.get(stopRoute);

            if (stopIndex < existingStopIndex) { // TODO: 17-Dec-18 I dont believe this
                activeRoutes.put(stopRoute, stopIndex);
            }
        } else {
            activeRoutes.put(stopRoute, stopIndex);
        }
    }

    /**
     * Collects all active routes for the current round. An active route is a route with an active stop.
     * Notes an index of the earliest marked stop on the route.
     */
    protected void collectActiveRoutes() {
        activeRoutes.clear();

        Iterator<Stop> markedStopsIt = markedStops.iterator();

        while (markedStopsIt.hasNext()) {
            Stop stop = markedStopsIt.next();

            List<Route> stopRoutes = stop.getRoutes();
            List<Integer> stopRoutesIndices = stop.getIndices();

            for (int i = 0; i < stopRoutes.size(); i++) {
                Route route = stopRoutes.get(i);
                int stopIndex = stopRoutesIndices.get( i);

                setEarliestStopIndex(route, stopIndex);
            }
            markedStopsIt.remove();
        }
    }

    /**
     * Finds an earliest trip that departs from a stop after the given arrival time.
     *
     * @param route a route containing the stop
     * @param previousArrival an arrival time
     * @param stopIndex an index of the stop on the route
     * @return an earliest trip departing after the given arrival time, null if trip is not found
     */
    private Trip findEarliestTrip(Route route, int previousArrival, int stopIndex) {
        List<Trip> routeTrips = route.getTrips();

        int earliestTripIndex = findEarliestTripIndex(route, previousArrival, stopIndex);
        if (earliestTripIndex < 0) {
            return null;
        }

        return routeTrips.get(earliestTripIndex);
    }

    private int findEarliestTripIndex(Route route, int previousArrival, int stopIndex) {
        List<Trip> routeTrips = route.getTrips();
        int left = 0;
        int right = routeTrips.size() - 1;
        int middle = 0;
        int departure = 0;

        if (routeTrips.size() == 0) {
//            System.err.println("Warning: Route " + route.getRouteId() + " (" + route.getId() + ") contains no trips.");
            return -1;
        }
        while (left <= right) {
            middle = (left + right) / 2;
            departure = routeTrips.get(middle).getDepartureAtStop(stopIndex);

            if (departure == previousArrival) {
                return middle;
            } else if (departure < previousArrival) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        if (middle == routeTrips.size() - 1) {    // out of bounds
            return -1;
        } else if (departure < previousArrival) { // next earliest departure is the next one
            return middle+1;
        } else {                                  // next earliest departure is the current one
            return middle;
        }
    }

    /**
     * Processes transfers of marked stops. Checks if any of the transfers can be used in the journey.
     */
    protected void processTransfers() {
        Set<Stop> markedStopsCopy = new HashSet<>(markedStops);

        for (Stop stop : markedStopsCopy) {
            List<Label> labelBagCopy = new ArrayList<>(eap.getLabelBag(stop, round)); // TODO: 17-Dec-18 this is not deepcopy
            for (Transfer transfer : stop.getTransfers()) {
                for (Label label : labelBagCopy) {
                    if (label.getTransfer() != null) {
                        continue; // TODO: 17-Dec-18 whats this?
                    }
                    int[] criteria = label.getCriteria();
                    int newArrival = criteria[0] + transfer.getTransferTime();
                    int price = criteria[1] + computeTransferPrice(transfer);
                    updateArrivalByTransfer(transfer, transfer.getTargetStop(), new int[]{newArrival, price}, label);
                }
            }
        }
    }

    private int computeTransferPrice(Transfer transfer) {
        if (transfer instanceof MultiTransfer) {
            MultiTransfer multiTransfer = (MultiTransfer) transfer;
            return multiTransfer.getPrice();
        }
        return 0; // since transfers are only by foot, it is for free
    }

    /**
     * Processes trips at active routes. Checks if any of the trips can be used in the journey.
     */
    protected void processActiveRoutes() {
        for (Map.Entry<Route, Integer> activeRoute : activeRoutes.entrySet()) {
            Route route = activeRoute.getKey();
            List<Stop> routeStops = route.getStops();
            List<Label> routeBag = new ArrayList<>();

            // process the route from the earliest marked stop
            for (int stopIndex = activeRoute.getValue(); stopIndex < routeStops.size(); stopIndex++) {

                Stop stop = routeStops.get(stopIndex);

                // update all arrival times of every label in routeBag according to their associated trips T(Label)
                for (Label label : routeBag) {
//                    int[] criteria = label.getCriteria();
                    Trip trip = label.getTrip();
                    if (trip != null) {
                        int arrivalAtStop = trip.getArrivalAtStop(stopIndex);
                        int tripPrice= computeTripPrice(trip, stopIndex, label);
                        if (tripPrice > 0) {
                            label.setTicketPurchased(true);
                        }
                        int price = label.getCriteria()[1] + tripPrice;
                        int[] newCriteria = new int[]{arrivalAtStop, price};
                        label.setCriteria(newCriteria);
//                        updateArrivalByTrip(trip, stop, label.getPrevStop(), newCriteria);
                    }
                }

                // add labels from routeBag into labelBag.
                List<Label> labelBag = eap.getLabelBag(stop, round);
                for (Label routeBagLabel : routeBag) {
                    // TODO: 19-Dec-18 this block could be added into previous loop
                    if (DominationUtils.anyPrevDominates(eap, stop, round, routeBagLabel)) {
                        continue;
                    }
                    if (!DominationUtils.anyDominates(labelBag, routeBagLabel)) {
                        Label newLabel = new Label(routeBagLabel);
                        labelBag.add(newLabel);
                        markedStops.add(stop);
                    }
                }
                // discard dominated labels in labelBag
                labelBag.removeIf(label -> DominationUtils.anyDominates(labelBag, label));

                //merge labelBag from previous round into routeBag
                List<Label> prevRoundLabelBag = eap.getLabelBag(stop, round - 1);
                for (Label prevLabel : prevRoundLabelBag) {
                    int[] prevLabelCriteria = prevLabel.getCriteria();
                    Trip prevTrip = prevLabel.getTrip();

//                    Trip bestTrip = findEarliestTrip(route, previousArrival, stopIndex);

                    List<Trip> bestTrips = findBestTrips(route, prevLabel, stopIndex);
//
                    for (Trip bestTrip : bestTrips) {
                        if (bestTrip != null && bestTrip != prevTrip) {
                            int prevArrivalTime = prevLabelCriteria[0];
                            int prevPrice = prevLabelCriteria[1];
                            Label label = new Label(new int[]{prevArrivalTime, prevPrice}, null, bestTrip, stop, prevLabel);//TODO prevStop vs stop
                            routeBag.add(label);
                        } else {
//                        routeBag.add(prevLabel);
                        }
                    }
                }
            }
        }
    }

    private List<Trip> findBestTrips(Route route, Label prevLabel, int stopIndex) {
        List<Trip> bestTrips = new ArrayList<>();
        int previousArrival = prevLabel.getCriteria()[0];
        int previousPrice = prevLabel.getCriteria()[1];

        int earliestTripIndex = findEarliestTripIndex(route, previousArrival, stopIndex);
        if (earliestTripIndex < 0) {
            return Collections.emptyList();
        }

        List<Trip> routeTrips = route.getTrips();
        Trip earliestTrip = routeTrips.get(earliestTripIndex);
        bestTrips.add(earliestTrip);

        int earliestTripPrice = previousPrice + computeTripPrice(earliestTrip, stopIndex, prevLabel);
        int counter = 0;
        // TODO: 15-Jan-19 influences the performance!!! try another approach to add following trips
        //idea 1: sort trips by price can't work, another sorting while reading the GTFS???
        //idea 2: use max following trips
        //idea 3: use max following time!
        for (int i = earliestTripIndex + 1; i < routeTrips.size(); i++) {
            counter++;
            if (counter > MAX_FOLLOWING_TRIPS) {
                break;
            }
            // TODO: 14-Jan-19 every trip costs the same, so this returns a list with one item
            Trip trip = routeTrips.get(i);
            int price = computeTripPrice(trip, stopIndex, prevLabel);

            if (price >= earliestTripPrice) {
                continue;
            }

            bestTrips.add(trip);
        }

        return bestTrips;
    }

    /**
     * The single-ticket is used for travelling. So, if the PT is used, then the single-ticket price is added.
     *
     * Note: It's really hard to implement general pricing for all cities
     * @param trip
     * @param stopIndex
     * @return
     */
    private int computeTripPrice(Trip trip, int stopIndex, Label label) {
        if (label == null || label.isTicketPurchased()) {
            return 0;
        }

        return SINGLE_TICKET_PRICE;
    }

    protected void updateArrivalByTransfer(Transfer transfer, Stop stop, int[] criteria, Label prevLabel) {
        Label label = new Label(criteria, transfer, null, null, prevLabel);

        if (DominationUtils.anyPrevDominates(eap, stop, round, label)) {
            // this stop was reached earlier (prev rounds) with criteria that dominates the actual criteria
            return;
        }

        List<Label> labelBag = eap.getLabelBag(stop, round);
        if (DominationUtils.anyDominates(labelBag, criteria)) {
            return;
        }

        labelBag.removeIf(l -> DominationUtils.dominates(label.getCriteria(), l.getCriteria()));
        eap.addLabelToBag(stop, round, label);
        markedStops.add(stop);
    }

    protected void updateArrivalByTrip(Trip trip, Stop stop, Stop pickUpStop, int[] criteria, Label prevLabel) {
        if (!DominationUtils.anyDominates(eap.getLabelBag(stop, round), criteria)) {
            Label label = new Label(criteria, null, trip, pickUpStop, prevLabel);
            eap.addLabelToBag(stop, round, label);
            markedStops.add(stop);
        }
    }

    /**
     * Collects all the trips and stops that make up the result journey.
     *
     * @return a result journey (a collection of trips and transfers to use for solving the EAP)
     */
    protected Map<Stop, List<Plan>> reconstructPlans() {
        System.out.println("Reconstructing path...");
        Map<Stop, List<Plan>> plans = new HashMap<>();
        int departureDateTime = eap.getDepartureDateTime();

        List<Stop> targetStops = eap.getTargetStops();
        if (targetStops == null) {
            targetStops = new ArrayList<>(eap.getStopData().keySet());
        }

        for (Stop targetStop : targetStops) {
            List<Plan> paretoSet = new ArrayList<>();

            for (int i = Raptor.MAX_TRIPS; i > -1; i--) {
                List<Label> labelBag = eap.getLabelBag(targetStop, i);

                if (labelBag == null || labelBag.isEmpty()) {
                    continue;
                }

                for (Label label : labelBag) {
                    int[] criteria = label.getCriteria();
                    int duration = criteria[0] - departureDateTime;
                    List<Leg> legs = new ArrayList<>();
                    Plan plan = new Plan(targetStop, duration, criteria[1], i, -1, -1, legs);//numOfTrips is Round index
                    paretoSet.add(plan);

                    if (!SHOULD_COMPUTE_LEGS) {
                        continue;
                    }

                    int numOfLegs = 0;
                    int numOfTrips = 0;
                    int numOfTransfers = 0;

                    Label tmpLabel = label;
                    while (tmpLabel != null) {
                        numOfLegs++;
                        Transfer transfer = tmpLabel.getTransfer();
                        Trip trip = tmpLabel.getTrip();
                        if ((transfer != null && trip != null) || (transfer == null && trip == null)) {
                            System.out.println("WHAT?");
                        }
                        if (transfer != null) {
                            numOfTransfers++;
                        }
                        if (trip != null) {
                            numOfTrips++;
                        }
                        // TODO: 15-Jan-19 add legs
//                        Leg leg = new Leg();
//                        legs.add(leg);
                        tmpLabel = tmpLabel.getPrevLabel();
                    }
                    plan.setNumOfTransfers(numOfTransfers);
                    plan.setNumOfTrips(numOfTrips);
                    plan.setNumOfLegs(numOfLegs);
                }
            }

            plans.put(targetStop, paretoSet);
        }

        return plans;
    }
}
