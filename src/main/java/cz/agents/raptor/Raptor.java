package cz.agents.raptor;

import cz.agents.raptor.problems.EarliestArrivalProblem;
import cz.agents.raptor.structures.*;
import java.util.*;


public class Raptor {
    public static final int MAX_TRIPS = 5; // maximal number of trips in the journey
    protected int round;                      // current number of trips
    protected EarliestArrivalProblem eap;
    protected Set<Stop> markedStops;
    protected HashMap<Route, Integer> activeRoutes;

    /**
     * Marks a stop and initializes its values using departure date and time.
     *
     * @param stop a stop to mark
     */
    private void markDepartureStop(Stop stop) {
        eap.setArrival(stop, 0, eap.getDepartureDateTime());
        eap.setEarliestArrival(stop, eap.getDepartureDateTime());
        markedStops.add(stop);
    }

    /**
     * Retrieves all neighbours of a stop (reachable by a transfer) and initializes them as the departure stops.
     *
     * @param stop a stop whose neighbours should be marked
     */
    private void markDepartureNeighbours(Stop stop) {
        for (Transfer transfer : stop.getTransfers()) {
            if (! transfer.getSourceStop().equals(stop)) {
                break; // TODO: 03-Dec-18 Here should be continue;
            }
            updateArrivalByTransfer(transfer, transfer.getTargetStop(), eap.getDepartureDateTime() + transfer.getTransferTime());
        }
    }

    /**
     * Sets the index of the earliest stop from which an active route should be processed.
     * If a higher index is already set for the route, it gets updated.
     *
     * @param stopRoute a route which should be active in the current round
     * @param stopIndex an index of a marked stop on the route
     */
    private void setEarliestStopIndex(Route stopRoute, int stopIndex) {
        if (activeRoutes.containsKey(stopRoute)) {
            int existingStopIndex = activeRoutes.get(stopRoute);

            if (stopIndex < existingStopIndex) {
                activeRoutes.put(stopRoute, stopIndex);
            }
        } else {
            activeRoutes.put(stopRoute, stopIndex);
        }
    }

    /**
     * Collects all active routes for the current round. An active route is a route with an active stop.
     * Notes an index of the earliest marked stop on the route.
     */
    protected void collectActiveRoutes() {
        activeRoutes.clear();

        Iterator<Stop> markedStopsIt = markedStops.iterator();

        while (markedStopsIt.hasNext()) {
            Stop stop = markedStopsIt.next();

            List<Route> stopRoutes = stop.getRoutes();
            List<Integer> stopRoutesIndices = stop.getIndices();

            for (int i = 0; i < stopRoutes.size(); i++) {
                Route route = stopRoutes.get(i);

                int stopIndex = stopRoutesIndices.get( i);
                setEarliestStopIndex(route, stopIndex);
            }
            markedStopsIt.remove();
        }
    }

    /**
     * Finds an earliest trip that departs from a stop after the given arrival time.
     *
     * @param route a route containing the stop
     * @param previousArrival an arrival time
     * @param stopIndex an index of the stop on the route
     * @return an earliest trip departing after the given arrival time, null if trip is not found
     */
    private Trip findEarliestTrip(Route route, int previousArrival, int stopIndex) {
        List<Trip> routeTrips = route.getTrips();
        int left = 0;
        int right = routeTrips.size() - 1;
        int middle = 0;
        int departure = 0;

        if (routeTrips.size() == 0) {
            System.err.println("Warning: Route " + route.getRouteId() + " (" + route.getId() + ") contains no trips.");
            return null;
        }
        while (left <= right) {
            middle = (left + right) / 2;
            departure = routeTrips.get(middle).getDepartureAtStop(stopIndex);

            if (departure == previousArrival) {
                return routeTrips.get(middle);
            } else if (departure < previousArrival) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        if (middle == routeTrips.size() - 1) {    // out of bounds
            return null;
        } else if (departure < previousArrival) { // next earliest departure is the next one
            return routeTrips.get(middle+1);
        } else {                                  // next earliest departure is the current one
            return routeTrips.get(middle);
        }
    }

    /**
     * Updates earliest arrival values if the stop can be reached sooner using the given transfer.
     *
     * @param transfer a transfer which can be used to get to the stop
     * @param stop a stop to update
     * @param newArrival an arrival time using the transfer
     */
    protected void updateArrivalByTransfer(Transfer transfer, Stop stop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop)
                && newArrival < eap.getEarliestArrival(eap.getTargetStop())) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTransfer(stop, round, transfer);
            markedStops.add(stop);
        }
    }

    /**
     * Updates earliest arrival values if the stop can be reached sooner using the given trip.
     *
     * @param trip a trip which can be used to get to the stop
     * @param stop a stop to update
     * @param newArrival an arrival time using the trip
     */
    protected void updateArrivalByTrip(Trip trip, Stop stop, Stop pickUpStop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop) && newArrival < eap.getEarliestArrival(eap.getTargetStop())) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTrip(stop, round, trip);
            eap.setPreviousStop(stop, round, pickUpStop);
            markedStops.add(stop);
        }
    }

    /**
     * Processes transfers of marked stops. Checks if any of the transfers can be used in the journey.
     */
    protected void processTransfers() {
        Set<Stop> markedStopsCopy = new HashSet<>(markedStops);

        for (Stop stop : markedStopsCopy) {
            if (eap.getTransfer(stop, round) != null) {
                continue;
            }
            for (Transfer transfer : stop.getTransfers()) {
                Stop transferTargetStop = transfer.getTargetStop();

                int newArrival = eap.getArrivalTime(stop, round) + transfer.getTransferTime();
                updateArrivalByTransfer(transfer, transferTargetStop, newArrival);
            }
        }
    }

    /**
     * Processes trips at active routes. Checks if any of the trips can be used in the journey.
     */
    protected void processActiveRoutes() {
        for (Map.Entry<Route, Integer> activeRoute : activeRoutes.entrySet()) {
            Trip currentTrip = null;
            Stop pickUpStop = null;
            Route route = activeRoute.getKey();
            List<Stop> routeStops = route.getStops();

            // process the route from the earliest marked stop
            for (int stopIndex = activeRoute.getValue(); stopIndex < routeStops.size(); stopIndex++) {
                Stop stop = routeStops.get(stopIndex);

                if (currentTrip != null) {
                    int newArrival = currentTrip.getArrivalAtStop(stopIndex);
                    updateArrivalByTrip(currentTrip, stop, pickUpStop, newArrival);
                }
                int previousArrival = eap.getArrivalTime(stop, round - 1);

                // possibility to find a new / earlier trip
                if (currentTrip == null || previousArrival < currentTrip.getDepartureAtStop(stopIndex)) {
                    Trip earliestTrip = findEarliestTrip(route, previousArrival, stopIndex);

                    // there is an earlier trip that can be used
                    if (earliestTrip != null && earliestTrip != currentTrip) {
                        currentTrip = earliestTrip;
                        pickUpStop = stop;
                    }
                }
            }
        }
    }

    /**
     * Finds out how many trips it took to get to the target stop.
     *
     * @return a number of trips that solves the earliest arrival problem
     */
    protected int getJourneyTripCount() {
        for (int i = round - 2; i >= 0; i--) {
            if (eap.getTransfer(eap.getTargetStop(), i) != null
                    || eap.getTrip(eap.getTargetStop(), i) != null) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Collects all the trips and stops that make up the result journey.
     *
     * @return a result journey (a collection of trips and transfers to use for solving the EAP)
     */
    protected Journey reconstructJourney() {
        Journey journey = new Journey();

        journey.setTimetableStartDate(eap.getTimetable().getStartDate());

        int tripCount = getJourneyTripCount();

        if (tripCount != -1) {
            Stop stop = eap.getTargetStop();

            for (int i = tripCount; i > 0; i--) {
                Transfer transfer = eap.getTransfer(stop, i);
                journey.addTransfer(transfer);

                if (transfer != null) {
                    stop = transfer.getSourceStop();
                }
                Trip trip = eap.getTrip(stop, i);
                journey.setDropOffStop(trip, stop); // the stop used to get out of the trip

                stop = eap.getPreviousStop(stop, i);
                journey.setPickUpStop(trip, stop);  // the stop used to hop on the trip

                journey.addTrip(trip);
            }
            Transfer transfer = eap.getTransfer(stop, 0);
            journey.addTransfer(transfer);

            return journey;
        } else {
            return null;
        }
    }

    /**
     * Takes an instance of the earliest arrival problem as an input and returns the journey solving this problem.
     *
     * @param eap an instance of the earliest arrival problem
     * @return a journey which solves eap, null if no journey is found
     */
    public Journey solve(EarliestArrivalProblem eap) {
        this.eap = eap;
        markedStops = new HashSet<>();
        activeRoutes = new HashMap<>();
        round = 0;

        markDepartureStop(eap.getSourceStop());
        markDepartureNeighbours(eap.getSourceStop());

        round++;

        while (!markedStops.isEmpty() && round <= MAX_TRIPS) {
            collectActiveRoutes();
            processActiveRoutes();
            processTransfers();

            round++;
        }

        return reconstructJourney();
    }
}
