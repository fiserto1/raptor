package cz.agents.raptor.load.gtfs;

import cz.agents.raptor.load.io.csv.ObjectFromCsvImporterFactory;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.load.io.imp.ObjectImporter.ExceptionPolicy;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.zip.ZipFile;

import static java.util.stream.Collectors.toSet;

/**
 * @author Marek Cuchý
 */
public class Gtfs {

	public static final Charset UTF_8 = Charset.forName("utf8");

	private static final Logger LOGGER = Logger.getLogger(Gtfs.class);

	private final Map<String, GtfsAgency> agencies = new LinkedHashMap<>();
	private final Map<String, GtfsRoutes> routes = new LinkedHashMap<>();
	private final Map<String, GtfsTrips> trips = new LinkedHashMap<>();
	private final Map<String, GtfsStops> stops = new LinkedHashMap<>();
	private final Map<String, GtfsCalendar> calendars = new LinkedHashMap<>();
	private final Map<String, List<GtfsCalendarDates>> calendarDates = new LinkedHashMap<>();
	private final Map<String, List<GtfsStopTimes>> stopTimes = new LinkedHashMap<>();

	/**
	 * Remove all elements inconsistent with the rest of the GTFS (e.g. stop times without corresponding stops or too
	 * short trips (only one stop)). It also removes unused elements.
	 */
	public void removeIllegalElements() {
		//remove routes without an agency
		routes.values().removeIf(r -> !agencies.containsKey(r.agencyId));

		//remove trips without a route
		trips.values().removeIf(t -> !routes.containsKey(t.routeId));

		//remove trips without a service (calendars or calendar dates)
		trips.values().removeIf(t -> !calendars.containsKey(t.serviceId) && !calendarDates.containsKey(t.serviceId));

		//remove stop times without a trip
		stopTimes.keySet().retainAll(trips.keySet());

		//remove stop times without a stop
		stopTimes.values().forEach(stopTime -> stopTime.removeIf(s -> !stops.containsKey(s.stopId)));

		//remove too short trips
		stopTimes.values().removeIf(s -> s.size() <= 1);

		//removed unused trips
		trips.keySet().retainAll(stopTimes.keySet());

		//remove unused services
		Set<String> usedServices = trips.values().stream().map(GtfsTrips::getServiceId).collect(toSet());
		calendars.keySet().retainAll(usedServices);
		calendarDates.keySet().retainAll(usedServices);

		//remove unused routes
		Set<String> usedRoutes = trips.values().stream().map(GtfsTrips::getRouteId).collect(toSet());
		routes.keySet().retainAll(usedRoutes);

		//remove unused stops
		Set<String> usedStops = stopTimes.values()
										 .stream()
										 .flatMap(Collection::stream)
										 .map(GtfsStopTimes::getStopId)
										 .collect(toSet());
		stops.keySet().retainAll(usedStops);

		//remove unused agencies
		Set<String> usedAgencies = routes.values().stream().map(GtfsRoutes::getAgencyId).collect(toSet());
		agencies.keySet().retainAll(usedAgencies);
	}

	/**
	 * Remove routes satisfying given {@code predicate}.
	 *
	 * @param predicate
	 */
	public void removeRoutes(Predicate<? super GtfsRoutes> predicate) {
		routes.values().removeIf(predicate);
	}

	/**
	 * Remove stops satisfying given {@code predicate}.
	 *
	 * @param predicate
	 */
	public void removeStops(Predicate<? super GtfsStops> predicate) {
		int size = stops.size();
		stops.values().removeIf(predicate);
		LOGGER.info("Removed " + (size - stops.size()) + " stops.");
	}

	public void addAgency(GtfsAgency agency) {
		if (agencies.containsKey(agency.agencyId))
			throw new IllegalArgumentException("Agency '" + agency.agencyId + "' already exists");
		agencies.put(agency.agencyId, agency);
	}

	public void addRoute(GtfsRoutes route) {
		if (routes.containsKey(route.routeId))
			throw new IllegalArgumentException("Route '" + route.routeId + "' already exists");
		routes.put(route.routeId, route);
	}

	public void addTrip(GtfsTrips trip) {
		if (routes.containsKey(trip.tripId))
			throw new IllegalArgumentException("Trip '" + trip.tripId + "' already exists");
		trips.put(trip.tripId, trip);
	}

	public void addStop(GtfsStops stop) {
		if (routes.containsKey(stop.stopId))
			throw new IllegalArgumentException("Stop '" + stop.stopId + "' already exists");
		stops.put(stop.stopId, stop);
	}

	public void addCalendar(GtfsCalendar calendar) {
		if (routes.containsKey(calendar.serviceId))
			throw new IllegalArgumentException("Calendar '" + calendar.serviceId + "' already exists");
		calendars.put(calendar.serviceId, calendar);
	}

	public void addCalendarDate(GtfsCalendarDates calendarDates) {
		this.calendarDates.computeIfAbsent(calendarDates.serviceId, k -> new ArrayList<>()).add(calendarDates);
	}

	public void addStopTime(GtfsStopTimes stopTime) {
		this.stopTimes.computeIfAbsent(stopTime.tripId, k -> new ArrayList<>()).add(stopTime);
	}

	public void addAgencies(Collection<? extends GtfsAgency> routes) {
		addAgencies(routes.stream());
	}

	public void addAgencies(Stream<? extends GtfsAgency> routes) {
		routes.forEach(this::addAgency);
	}

	public void addRoutes(Collection<? extends GtfsRoutes> routes) {
		addRoutes(routes.stream());
	}

	public void addRoutes(Stream<? extends GtfsRoutes> routes) {
		routes.forEach(this::addRoute);
	}

	public void addTrips(Collection<? extends GtfsTrips> routes) {
		addTrips(routes.stream());
	}

	public void addTrips(Stream<? extends GtfsTrips> routes) {
		routes.forEach(this::addTrip);
	}

	public void addStops(Collection<? extends GtfsStops> stops) {
		addStops(stops.stream());
	}

	public void addStops(Stream<? extends GtfsStops> stops) {
		stops.forEach(this::addStop);
	}

	public void addCalendars(Collection<? extends GtfsCalendar> calendars) {
		addCalendars(calendars.stream());
	}

	public void addCalendars(Stream<? extends GtfsCalendar> calendars) {
		calendars.forEach(this::addCalendar);
	}

	public void addCalendarDates(Collection<? extends GtfsCalendarDates> calendarDates) {
		addCalendarDates(calendarDates.stream());
	}

	public void addCalendarDates(Stream<? extends GtfsCalendarDates> calendarDates) {
		calendarDates.forEach(this::addCalendarDate);
	}

	public void addStopTimes(Collection<? extends GtfsStopTimes> stopTimes) {
		addStopTimes(stopTimes.stream());
	}

	public void addStopTimes(Stream<? extends GtfsStopTimes> stopTimes) {
		stopTimes.forEach(this::addStopTime);
	}

	public boolean containsAgency(String agencyId) {
		return agencies.containsKey(agencyId);
	}

	public boolean containsRoute(String routeId) {
		return routes.containsKey(routeId);
	}

	public boolean containsTrip(String tripId) {
		return trips.containsKey(tripId);
	}

	public boolean containsStop(String stopId) {
		return stops.containsKey(stopId);
	}

	public boolean containsCalendar(String serviceId) {
		return calendars.containsKey(serviceId);
	}

	public boolean containsCalendarDates(String serviceId) {
		return calendarDates.containsKey(serviceId);
	}

	public boolean containsStopTimes(String tripId) {
		return stopTimes.containsKey(tripId);
	}

	public Map<String, GtfsAgency> getAgencies() {
		return agencies;
	}

	public Map<String, GtfsRoutes> getRoutes() {
		return routes;
	}

	public Map<String, GtfsTrips> getTrips() {
		return trips;
	}

	public Map<String, GtfsStops> getStops() {
		return stops;
	}

	public Map<String, GtfsCalendar> getCalendars() {
		return calendars;
	}

	public Map<String, List<GtfsCalendarDates>> getCalendarDates() {
		return calendarDates;
	}

	public Map<String, List<GtfsStopTimes>> getStopTimes() {
		return stopTimes;
	}

	public static Gtfs parseFolder(String folder) throws ObjectImportException {
		ObjectFromCsvImporterFactory factory = new ObjectFromCsvImporterFactory(',', UTF_8, true);
		Gtfs gtfs = new Gtfs();
		gtfs.addAgencies(
				factory.createImporterFromExactPath(GtfsAgency.class, folder + "agency.txt", ExceptionPolicy.NULL)
					   .stream());
		gtfs.addCalendars(factory.createImporterFromExactPath(GtfsCalendar.class, folder + "calendar.txt").stream());
		gtfs.addCalendarDates(
				factory.createImporterFromExactPath(GtfsCalendarDates.class, folder + "calendar_dates.txt").stream());
		gtfs.addRoutes(
				factory.createImporterFromExactPath(GtfsRoutes.class, folder + "routes.txt", ExceptionPolicy.NULL)
					   .stream());
		gtfs.addStops(factory.createImporterFromExactPath(GtfsStops.class, folder + "stops.txt", ExceptionPolicy.NULL)
							 .stream());
		gtfs.addStopTimes(factory.createImporterFromExactPath(GtfsStopTimes.class, folder + "stop_times.txt",
															  ExceptionPolicy.NULL).stream());
		gtfs.addTrips(factory.createImporterFromExactPath(GtfsTrips.class, folder + "trips.txt", ExceptionPolicy.NULL)
							 .stream());

		return gtfs;
	}

	public static Gtfs parseZip(String zipPath) throws ObjectImportException {
		ZipFile zipFile;
		try {
			zipFile = new ZipFile(zipPath);
		} catch (IOException e) {
			throw new ObjectImportException("ZIP file can't be opened", e);
		}

		ObjectFromCsvImporterFactory factory = new ObjectFromCsvImporterFactory(',', UTF_8, true);
		Gtfs gtfs = new Gtfs();
		gtfs.addAgencies(read(zipFile, factory, "agency.txt", GtfsAgency.class, ExceptionPolicy.NULL));
		gtfs.addCalendars(read(zipFile, factory, "calendar.txt", GtfsCalendar.class, ExceptionPolicy.THROW));
		gtfs.addCalendarDates(
				read(zipFile, factory, "calendar_dates.txt", GtfsCalendarDates.class, ExceptionPolicy.THROW));
		gtfs.addRoutes(read(zipFile, factory, "routes.txt", GtfsRoutes.class, ExceptionPolicy.NULL));
		gtfs.addStops(read(zipFile, factory, "stops.txt", GtfsStops.class, ExceptionPolicy.NULL));
		gtfs.addStopTimes(read(zipFile, factory, "stop_times.txt", GtfsStopTimes.class, ExceptionPolicy.NULL));
		gtfs.addTrips(read(zipFile, factory, "trips.txt", GtfsTrips.class, ExceptionPolicy.NULL));

		return gtfs;
	}

	private static <T> Stream<T> read(ZipFile zipFile, ObjectFromCsvImporterFactory factory, String fileName,
									  Class<T> clazz, ExceptionPolicy exceptionPolicy) throws ObjectImportException {
		return factory.createFromInputStream(clazz, toInputStream(zipFile, fileName), exceptionPolicy).stream();
	}

	private static InputStream toInputStream(ZipFile zipFile, String fileName) throws ObjectImportException {
		try {
			return zipFile.getInputStream(zipFile.getEntry(fileName));
		} catch (IOException e) {
			throw new ObjectImportException("ZIP not complete.", e);
		}
	}

	@Override
	public String toString() {
		return "Gtfs [" + "agencies=" + agencies.size() + ", routes=" + routes.size() + ", trips=" + trips.size() +
			   ", stops=" + stops.size() + ", calendars=" + calendars.size() + ", calendarDates=" +
			   calendarDates.values().stream().mapToInt(Collection::size).sum() + ", stopTimes=" +
			   stopTimes.values().stream().mapToInt(Collection::size).sum() + ']';
	}
}
