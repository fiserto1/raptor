package cz.agents.raptor.load.gtfs;

import cz.agents.raptor.load.io.csv.CsvConvert;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 * @author Marek Cuchý
 */
public class GtfsCalendar {

	@Id
	public final String serviceId;

	public final boolean monday;
	public final boolean tuesday;
	public final boolean wednesday;
	public final boolean thursday;
	public final boolean friday;
	public final boolean saturday;
	public final boolean sunday;

	@CsvConvert(converter = GtfsDateConverter.class)
	@Column(nullable = false)
	public final LocalDate startDate;

	@CsvConvert(converter = GtfsDateConverter.class)
	@Column(nullable = false)
	public final LocalDate endDate;

	private GtfsCalendar() {
		this(null, false, false, false, false, false, false, false, null, null);
	}

	public GtfsCalendar(String serviceId, boolean monday, boolean tuesday, boolean wednesday, boolean thursday,
						boolean friday, boolean saturday, boolean sunday, LocalDate startDate, LocalDate endDate) {
		this.serviceId = serviceId;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.startDate = startDate;
		this.endDate = endDate;
	}
}
