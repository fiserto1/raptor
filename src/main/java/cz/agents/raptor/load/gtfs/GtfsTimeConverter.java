package cz.agents.raptor.load.gtfs;

import cz.agents.raptor.tools.TimeHelper;

import javax.persistence.AttributeConverter;

/**
 * @author Marek Cuchý
 */
public class GtfsTimeConverter implements AttributeConverter<Integer,String> {

	public String convertToDatabaseColumn(Integer attribute) {
		return TimeHelper.formatTime(attribute, "HH:mm:ss");
	}

	public Integer convertToEntityAttribute(String dbData) {
		return TimeHelper.getTimeInSeconds(dbData);
	}
}
