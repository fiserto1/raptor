package cz.agents.raptor.load.io;

import cz.agents.raptor.load.io.source.AttributeFieldSource;
import cz.agents.raptor.load.io.source.AttributeSourceFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

/**
 * Utility class for object import/exp
 *
 * @author Marek Cuchý
 */
public class ImpExpUtils {

	private static final AttributeSourceFactory ATTRIBUTE_SOURCE_FACTORY = new AttributeSourceFactory();

	/**
	 * Create {@link AttributeFieldSource} for every field in the
	 * {@code clazz} and for every field in every superclass in class hierarchy.
	 *
	 * @param clazz
	 *
	 * @return
	 *
	 * @throws ReflectiveOperationException
	 */
	public static List<AttributeFieldSource> createAttributeFieldSourceForAllFieldsIncludingSuperClasses(
			Class<?> clazz) throws ReflectiveOperationException {
		List<AttributeFieldSource> sources = new ArrayList<>();

		List<Class<?>> superclasses = getAllSuperclasses(clazz);

		// add all fields from superclasses
		for (Class<?> superclass : superclasses) {
			sources.addAll(createAttributeFieldSourceForAllFields(superclass));
		}

		// add fields from clazz
		sources.addAll(createAttributeFieldSourceForAllFields(clazz));

		return sources;
	}

	/**
	 * Create {@link AttributeFieldSource} for every field in the {@code clazz}.
	 *
	 * @param clazz
	 *
	 * @return
	 *
	 * @throws ReflectiveOperationException
	 */
	protected static List<AttributeFieldSource> createAttributeFieldSourceForAllFields(
			Class<?> clazz) throws ReflectiveOperationException {
		List<AttributeFieldSource> sources = new ArrayList<>();

		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (Modifier.isStatic(field.getModifiers())) continue;
			String name = field.getName();
			String outputName = getOutputName(name);
			Class<?> columnType = field.getType();
			AttributeFieldSource source = ATTRIBUTE_SOURCE_FACTORY.createAttributeFieldSource(clazz, name, outputName,
																							  null, columnType);
			sources.add(source);
		}
		return sources;
	}

	/**
	 * Converts camel case to lower case words split by underscore character
	 *
	 * @param original
	 *
	 * @return
	 */
	public static String getOutputName(String original) {
		StringBuilder output = new StringBuilder();
		String[] split = original.split("(?=[A-Z])");

		for (String s : split) {
			output.append(s).append("_");
		}

		output.deleteCharAt(output.length() - 1);

		return output.toString().toLowerCase();
	}

	/**
	 * Get all superclasses of {@code clazz} ordered from the highest class.
	 *
	 * @param clazz
	 *
	 * @return
	 */
	static List<Class<?>> getAllSuperclasses(Class<?> clazz) {
		LinkedList<Class<?>> classes = new LinkedList<>();

		Class<?> superclass = clazz.getSuperclass();

		while (superclass != null) {
			classes.addFirst(superclass);
			superclass = superclass.getSuperclass();
		}

		return classes;
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	public static Function<String, Object> createEnumConverter(Class<?> type) throws NoSuchMethodException {
		if (!type.isEnum()) throw new IllegalArgumentException("Type is not an enum: " + type);
		Class<Enum> enumType = (Class<Enum>) type;
		return v -> Enum.valueOf(enumType, v.toUpperCase());
	}
}
