package cz.agents.raptor.load.io.csv;

import com.opencsv.CSVReader;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.load.io.imp.ObjectImporter;
import cz.agents.raptor.load.io.source.AttributeFieldSource;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

/**
 * @author Marek Cuchý
 */
public class ObjectFromCsvImporter<T> extends ObjectImporter<T> {

	protected final Map<String, Integer> columnNameIndexes;
	protected final Map<String, Function<String, ?>> columnConverters;

	protected final Iterator<String[]> iterator;
	protected final String lineEnd;

	public ObjectFromCsvImporter(List<AttributeFieldSource> fields, Class<T> dataClass,
								 Map<String, Integer> columnNameIndexes,
								 Map<String, Function<String, ?>> columnConverters, CSVReader reader,
								 String lineEnd, ExceptionPolicy exceptionPolicy) throws ObjectImportException {
		super(fields, dataClass, exceptionPolicy);
		this.columnNameIndexes = columnNameIndexes;
		this.columnConverters = columnConverters;
		this.iterator = reader.iterator();
		this.lineEnd = lineEnd;
	}

	@Override
	protected boolean sourceHasNext() {
		return iterator.hasNext();
	}

	@Override
	protected ObjectImportAttributes readAttributes() {
		ObjectImportAttributes attributes = new ObjectImportAttributes();
		String[] line = iterator.next();
		line[line.length - 1] = modifyLastColumn(line[line.length - 1]);

		for (Entry<String, Integer> entry : columnNameIndexes.entrySet()) {
			String name = entry.getKey();
			Integer index = entry.getValue();
			Function<String, ?> converter = columnConverters.get(name);
			try {
				if (index < 0) index = line.length + index;
//				if (index >= line.length) {
//					attributes.addAttribute(name, converter.apply(""));
//					continue;
//				}
				attributes.addAttribute(name, converter.apply(line[index]));
			} catch (IllegalArgumentException | IllegalStateException e) {
				attributes.addException(name, e);
			}
		}
		return attributes;
	}

	private String modifyLastColumn(String column) {
		if (column.endsWith(lineEnd)) {
			return column.substring(0, column.lastIndexOf(lineEnd));
		}
		return column;
	}

}
