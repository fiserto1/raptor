package cz.agents.raptor.load.io.imp;

/**
 * @author Marek Cuchý
 */
public class ObjectImportException extends Exception {

	private static final long serialVersionUID = 5076090333194571551L;

	public ObjectImportException() {
	}

	public ObjectImportException(String message) {
		super(message);
	}

	public ObjectImportException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectImportException(Throwable cause) {
		super(cause);
	}
}
