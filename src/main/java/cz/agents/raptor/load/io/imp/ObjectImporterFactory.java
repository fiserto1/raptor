package cz.agents.raptor.load.io.imp;

import cz.agents.raptor.load.io.ImpExpUtils;

/**
 * @author Marek Cuchý
 */
public abstract class ObjectImporterFactory {

	protected final String prefix;
	protected final String postfix;

	public ObjectImporterFactory() {
		this("", "");
	}

	public ObjectImporterFactory(String prefix, String postfix) {
		this.prefix = prefix;
		this.postfix = postfix;
	}

	public <T> ObjectImporter<T> createImporter(Class<T> clazz) throws ObjectImportException {
		return createImporter(clazz, prefix + ImpExpUtils.getOutputName(clazz.getSimpleName() + postfix));
	}

	public abstract <T> ObjectImporter<T> createImporter(Class<T> clazz, String sourceName) throws ObjectImportException;
}
