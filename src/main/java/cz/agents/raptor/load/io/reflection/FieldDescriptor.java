package cz.agents.raptor.load.io.reflection;

import java.lang.reflect.Field;

/**
 *
 * @author Marek Cuchy
 *
 */
public class FieldDescriptor extends ReflectionSourceDescriptor<Field> {

    public FieldDescriptor(Class<?> clazz, String fieldName) {
	super(clazz, fieldName);
    }

    @Override
    public Field getReflectionObject() throws ReflectiveOperationException {
	Field field = clazz.getDeclaredField(name);
	field.setAccessible(true);
	return field;
    }

    @Override
    public Class<?> getReflectionValueType() throws ReflectiveOperationException {
	return getReflectionObject().getType();
    }

}
