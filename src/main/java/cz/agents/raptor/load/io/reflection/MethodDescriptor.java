package cz.agents.raptor.load.io.reflection;

import java.lang.reflect.Method;

/**
 * 
 * @author Marek Cuchy
 * 
 */
public class MethodDescriptor extends ReflectionSourceDescriptor<Method> {

	public final Class<?>[] methodAttributeTypes;

	public MethodDescriptor(Class<?> clazz, String methodName, Class<?>... methodAttributeTypes) {
		super(clazz, methodName);
		this.methodAttributeTypes = methodAttributeTypes;
	}

	@Override
	public Method getReflectionObject() throws ReflectiveOperationException {
		Method method = clazz.getDeclaredMethod(name, methodAttributeTypes);
		method.setAccessible(true);
		return method;
	}

	@Override
	public Class<?> getReflectionValueType() throws ReflectiveOperationException {
		return getReflectionObject().getReturnType();
	}

}
