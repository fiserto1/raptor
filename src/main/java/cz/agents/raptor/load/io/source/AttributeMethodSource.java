package cz.agents.raptor.load.io.source;

import cz.agents.raptor.load.io.reflection.MethodDescriptor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Holds {@code Method} and superiorComponent and children in the reflection
 * structure.<br/>
 * Provides method for gathering value from the method.
 * 
 * @author Marek Cuchy
 * 
 */
public class AttributeMethodSource extends AttributeSource {

	private Method method;
	private Object[] arguments;

	/**
	 * 
	 * 
	 * @param parent
	 *            in reflection structure, if this instance is root the
	 *            superiorComponent should be {@code null}
	 * @param outputName
	 * @param correspondingClass
	 * @param method
	 * @param arguments
	 */
	public AttributeMethodSource(AttributeSource parent, String outputName, Class<?> correspondingClass, Method method,
	        Object... arguments) {
		super(parent, outputName, correspondingClass);
		this.method = method;
		this.arguments = arguments;
	}

	public AttributeMethodSource(AttributeSource parent, String name, Class<?> correspondingClass,
								 MethodDescriptor methodDescriptor, Object... arguments) throws ReflectiveOperationException {
		this(parent, name, correspondingClass, methodDescriptor.getReflectionObject(), arguments);
	}

	@Override
	protected Object getValueFromSource(Object source) throws IllegalArgumentException, IllegalAccessException,
															  InvocationTargetException {
		return method.invoke(source, arguments);
	}
}
