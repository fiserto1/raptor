package cz.agents.raptor.problems;

import cz.agents.raptor.Raptor;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.TimeHelper;

import java.time.LocalDate;
import java.util.*;

/**
 * This class represents an instance of the Earliest Arrival problem.
 * Given a source stop, a target stop and a departure time, the task is to find
 * a journey that departs from the source stop after the departure time
 * and arrives at the target stop as early as possible.
 */
public class EarliestArrivalProblem {
    private final Timetable timetable;
    private final Stop sourceStop;
    private final Stop targetStop;
    private final int departureDateTime;
    private Map<Stop, StopData> stopData;

    public class StopData {
        // an absolute earliest arrival to this stop
        public int earliestArrivalTime;

        // a list of earliest arrivals to this stop (for each round)
        Integer[] earliestArrivalTimeList;

        // a list of trips that should be used in the journey to reach this stop (for each round)
        Trip[] tripList;

        // a list of transfers that should be used in the journey to reach this stop (for each round)
        Transfer[] transferList;

        // a list of stops prior to this one in the journey (for each round)
        Stop[] previousStopList;

        StopData() {
            this.earliestArrivalTimeList = new Integer[Raptor.MAX_TRIPS + 1];
            this.tripList = new Trip[Raptor.MAX_TRIPS + 1];
            this.transferList = new Transfer[Raptor.MAX_TRIPS + 1];
            this.previousStopList = new Stop[Raptor.MAX_TRIPS + 1];
            this.earliestArrivalTime = TimeHelper.INFINITY;

            Arrays.fill(earliestArrivalTimeList, TimeHelper.INFINITY);
        }
    }

    /**
     * Creates a new instance of the Earliest Arrival Problem.
     *
     * @param timetable     a timetable describing the set of routes, stops, trips and transfers.
     * @param sourceStop    the source stop of the journey.
     * @param targetStop    the target stop of the journey.
     * @param departureDate the date of the departure
     * @param departureTime the earliest possible departure time from the source stop.
     */
    public EarliestArrivalProblem(Timetable timetable, Stop sourceStop, Stop targetStop, LocalDate departureDate, int departureTime) {
        this.timetable = timetable;
        this.sourceStop = sourceStop;
        this.targetStop = targetStop;
        this.departureDateTime = TimeHelper.secondsBetween(timetable.getStartDate(), departureDate) + departureTime;
        stopData = new HashMap<>();
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public Stop getSourceStop() {
        return sourceStop;
    }

    public Stop getTargetStop() {
        return targetStop;
    }

    public int getDepartureDateTime() {
        return departureDateTime;
    }

    public int getArrivalTime(Stop stop, int index) {
        return this.getStopData(stop).earliestArrivalTimeList[index];
    }

    public int getEarliestArrival(Stop stop) {
        return this.getStopData(stop).earliestArrivalTime;
    }

    public Map<Stop, StopData> getStopData() {
        return this.stopData;
    }

    public Stop getPreviousStop(Stop stop, int index) {
        return this.getStopData(stop).previousStopList[index];
    }

    public Trip getTrip(Stop stop, int index) {
        return this.getStopData(stop).tripList[index];
    }

    public Transfer getTransfer(Stop stop,int index) {
        return this.getStopData(stop).transferList[index];
    }

    private StopData getStopData(Stop stop) {
        if (stopData.containsKey(stop)) {
            return stopData.get(stop);
        }
        StopData data = new StopData();
        stopData.put(stop, data);

        return data;
    }
    public void setArrival(Stop stop, int index, int arrivalTime) {
        this.getStopData(stop).earliestArrivalTimeList[index] = arrivalTime;
    }

    public void setEarliestArrival(Stop stop, int earliestArrivalTime) {
        this.getStopData(stop).earliestArrivalTime = earliestArrivalTime;
    }

    public void setPreviousStop(Stop stop, int index, Stop previousStop) {
        this.getStopData(stop).previousStopList[index] = previousStop;
    }

    public void setTrip(Stop stop, int index, Trip trip) {
        this.getStopData(stop).tripList[index] = trip;
    }

    public void setTransfer(Stop stop, int index, Transfer transfer) {
        this.getStopData(stop).transferList[index] = transfer;
    }

}
