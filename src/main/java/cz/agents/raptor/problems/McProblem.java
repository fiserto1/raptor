package cz.agents.raptor.problems;

import cz.agents.basestructures.GPSLocation;
import cz.agents.common.TransportMode;
import cz.agents.raptor.Raptor;
import cz.agents.raptor.structures.Label;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.TimeHelper;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class McProblem {

    private final Timetable timetable;
    private final Set<TransportMode> allowedModes;
    private GPSLocation sourceLocation;
    private final List<Stop> targetStops;
    private final int departureDateTime;

    private Map<Stop, StopData> stopData;
    private List<Transfer> firstMileTransfers;

    public class StopData {
        private List<List<Label>> roundLabelBag;

        StopData() {
            this.roundLabelBag = new ArrayList<>(Raptor.MAX_TRIPS + 1);
            for (int i = 0; i < Raptor.MAX_TRIPS + 1; i++) {
                this.roundLabelBag.add(new ArrayList<>());
            }
        }
    }

    public McProblem(Timetable timetable, GPSLocation sourceLocation, List<Stop> targetStops, int departureDateTime, Set<TransportMode> allowedModes) {
        this.timetable = timetable;
        this.sourceLocation = sourceLocation;
        this.targetStops = targetStops;
        this.departureDateTime = departureDateTime;
        this.firstMileTransfers = new ArrayList<>();
        this.stopData = new HashMap<>();
        this.allowedModes = allowedModes;
    }

    public Set<TransportMode> getAllowedModes() {
        return allowedModes;
    }
    public List<Transfer> getFirstMileTransfers() {
        return firstMileTransfers;
    }

    public void setFirstMileTransfers(List<Transfer> firstMileTransfers) {
        this.firstMileTransfers = firstMileTransfers;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public GPSLocation getSourceLocation() {
        return sourceLocation;
    }

    public int getDepartureDateTime() {
        return departureDateTime;
    }

    public List<Stop> getTargetStops() {
        return targetStops;
    }

    public Map<Stop, StopData> getStopData() {
    return this.stopData;
}

    public StopData getStopData(Stop stop) {
        if (stopData.containsKey(stop)) {
            return stopData.get(stop);
        }
        StopData data = new StopData();
        stopData.put(stop, data);

        return data;
    }

    public List<Label> getLabelBag(Stop stop, int roundIndex) {
        return this.getStopData(stop).roundLabelBag.get(roundIndex);
    }

    public void addLabelToBag(Stop stop, int roundIndex, Label label) {
        this.getStopData(stop).roundLabelBag.get(roundIndex).add(label);
    }
}
