package cz.agents.raptor.problems;

import cz.agents.basestructures.GPSLocation;
import cz.agents.common.TransportMode;
import cz.agents.raptor.Raptor;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.TimeHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiEarliestArrivalProblem {

    private final Timetable timetable;
    private final Set<TransportMode> allowedModes;
    private GPSLocation sourceLocation;
    private final List<Stop> targetStops;
    private final int departureDateTime;


    private Map<Stop, StopData> stopData;
    private List<Transfer> firstMileTransfers;

    public class StopData {

        // an absolute earliest arrival to this stop
        public int earliestArrivalTime;
        // a list of earliest arrivals to this stop (for each round)

        public Integer[] earliestArrivalTimeList;
        // a list of trips that should be used in the journey to reach this stop (for each round)

        public Trip[] tripList;
        // a list of transfers that should be used in the journey to reach this stop (for each round)

        public Transfer[] transferList;
        // a list of stops prior to this one in the journey (for each round)

        public Stop[] previousStopList;
        StopData() {
            this.earliestArrivalTimeList = new Integer[Raptor.MAX_TRIPS + 1];
            this.tripList = new Trip[Raptor.MAX_TRIPS + 1];
            this.transferList = new Transfer[Raptor.MAX_TRIPS + 1];
            this.previousStopList = new Stop[Raptor.MAX_TRIPS + 1];
            this.earliestArrivalTime = TimeHelper.INFINITY;

            Arrays.fill(earliestArrivalTimeList, TimeHelper.INFINITY);
        }

        /**
         * Finds out how many trips it took to get to the target stop.
         *
         * @return a number of trips that solves the earliest arrival problem
         */
        public int getTripCount() {
            for (int i = Raptor.MAX_TRIPS; i >= 0; i--) {
                if (transferList[i] != null
                        || tripList[i] != null) {
                    return i;
                }
            }
            return -1;
        }

    }
    public MultiEarliestArrivalProblem(Timetable timetable, GPSLocation sourceLocation, List<Stop> targetStops, int departureDateTime, Set<TransportMode> allowedModes) {
        this.timetable = timetable;
        this.sourceLocation = sourceLocation;
        this.targetStops = targetStops;
        this.departureDateTime = departureDateTime;
        this.firstMileTransfers = new ArrayList<>();
        this.stopData = new HashMap<>();
        this.allowedModes = allowedModes;
    }

    public Set<TransportMode> getAllowedModes() {
        return allowedModes;
    }

    public List<Transfer> getFirstMileTransfers() {
        return firstMileTransfers;
    }

    public void setFirstMileTransfers(List<Transfer> firstMileTransfers) {
        this.firstMileTransfers = firstMileTransfers;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public GPSLocation getSourceLocation() {
        return sourceLocation;
    }

    public List<Stop> getTargetStops() {
        return targetStops;
    }

    public int getDepartureDateTime() {
        return departureDateTime;
    }

    public int getArrivalTime(Stop stop, int index) {
        return this.getStopData(stop).earliestArrivalTimeList[index];
    }

    public int getEarliestArrival(Stop stop) {
        return this.getStopData(stop).earliestArrivalTime;
    }

    public Map<Stop, StopData> getStopData() {
        return this.stopData;
    }

    public Stop getPreviousStop(Stop stop, int index) {
        return this.getStopData(stop).previousStopList[index];
    }

    public Trip getTrip(Stop stop, int index) {
        return this.getStopData(stop).tripList[index];
    }

    public Transfer getTransfer(Stop stop,int index) {
        return this.getStopData(stop).transferList[index];
    }

    public StopData getStopData(Stop stop) {
        if (stopData.containsKey(stop)) {
            return stopData.get(stop);
        }
        StopData data = new StopData();
        stopData.put(stop, data);

        return data;
    }



    public void setArrival(Stop stop, int index, int arrivalTime) {
        this.getStopData(stop).earliestArrivalTimeList[index] = arrivalTime;
    }

    public void setEarliestArrival(Stop stop, int earliestArrivalTime) {
        this.getStopData(stop).earliestArrivalTime = earliestArrivalTime;
    }

    public void setPreviousStop(Stop stop, int index, Stop previousStop) {
        this.getStopData(stop).previousStopList[index] = previousStop;
    }

    public void setTrip(Stop stop, int index, Trip trip) {
        this.getStopData(stop).tripList[index] = trip;
    }

    public void setTransfer(Stop stop, int index, Transfer transfer) {
        this.getStopData(stop).transferList[index] = transfer;
    }

}
