package cz.agents.raptor;

import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Transfer;
import cz.agents.raptor.structures.Trip;

public class rRaptor extends Raptor {

    @Override
    protected void updateArrivalByTransfer(Transfer transfer, Stop stop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop)) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTransfer(stop, round, transfer);
            markedStops.add(stop);
        }
    }

    @Override
    protected void updateArrivalByTrip(Trip trip, Stop stop, Stop pickUpStop, int newArrival) {
        if (newArrival < eap.getEarliestArrival(stop)) {
            eap.setEarliestArrival(stop, newArrival);
            eap.setArrival(stop, round, newArrival);
            eap.setTrip(stop, round, trip);
            eap.setPreviousStop(stop, round, pickUpStop);
            markedStops.add(stop);
        }
    }

    /**
     * Finds out how many trips it took to get to the target stop.
     *
     * @return a number of trips that solves the earliest arrival problem
     */
    protected int getJourneyTripCount() {
        return -1;
    }

    /**
     * Collects all the trips and stops that make up the result journey.
     *
     * @return a result journey (a collection of trips and transfers to use for solving the EAP)
     */
    @Override
    protected Journey reconstructJourney() {
        return null;
    }
}
