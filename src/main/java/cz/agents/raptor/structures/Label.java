package cz.agents.raptor.structures;

import java.util.Arrays;
import java.util.Objects;

public class Label {
    private int[] criteria;
    private Transfer transfer;
    private Trip trip;
    private Stop prevStop;
    private Label prevLabel;

    /**
     * True if the PT ticket is purchased
     * // TODO: 15-Jan-19 change it for object Ticket to generalize prices
     */
    private boolean ticketPurchased;

    public Label(Label label) {
        int[] originCriteria = label.getCriteria();
        this.criteria = new int[originCriteria.length];
        System.arraycopy(originCriteria, 0, this.criteria, 0, originCriteria.length);
        this.transfer = label.getTransfer();
        this.trip = label.getTrip();
        this.prevStop = label.getPrevStop();
        this.prevLabel = label.getPrevLabel();
        this.ticketPurchased = label.ticketPurchased;
    }

    public Label(int[] criteria, Transfer transfer, Trip trip, Stop prevStop, Label prevLabel) {
        this.criteria = criteria;
        this.transfer = transfer;
        this.trip = trip;
        this.prevStop = prevStop;
        this.prevLabel = prevLabel;
        this.ticketPurchased = prevLabel != null && prevLabel.isTicketPurchased();
    }

    public int[] getCriteria() {
        return criteria;
    }

    public void setCriteria(int[] criteria) {
        this.criteria = criteria;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Stop getPrevStop() {
        return prevStop;
    }

    public void setPrevStop(Stop prevStop) {
        this.prevStop = prevStop;
    }

    public Label getPrevLabel() {
        return prevLabel;
    }

    public void setPrevLabel(Label prevLabel) {
        this.prevLabel = prevLabel;
    }

    public boolean isTicketPurchased() {
        return ticketPurchased;
    }

    public void setTicketPurchased(boolean ticketPurchased) {
        this.ticketPurchased = ticketPurchased;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Label label = (Label) o;
        return ticketPurchased == label.ticketPurchased &&
                Arrays.equals(criteria, label.criteria) &&
                Objects.equals(prevStop, label.prevStop);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(prevStop, ticketPurchased);
        result = 31 * result + Arrays.hashCode(criteria);
        return result;
    }

    @Override
    public String toString() {
        return "Label{" +
                "criteria=" + Arrays.toString(criteria) +
                ", ticketPurchased=" + ticketPurchased +
                '}';
    }
}
