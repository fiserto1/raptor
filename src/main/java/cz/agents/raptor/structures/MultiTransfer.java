package cz.agents.raptor.structures;

import cz.agents.common.TransportMode;

import java.util.List;
import java.util.Objects;

public class MultiTransfer extends Transfer {
    private List<TransportMode> modes;
    private int price;

    public MultiTransfer(Stop sourceStop, Stop targetStop, int transferTime, List<TransportMode> modes, int price) {
        super(sourceStop, targetStop, transferTime);
        this.modes = modes;
        this.price = price;
    }

    public MultiTransfer(Stop sourceStop, Stop targetStop, int transferTime) {
        super(sourceStop, targetStop, transferTime);
    }

    public List<TransportMode> getModes() {
        return modes;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultiTransfer that = (MultiTransfer) o;
        return price == that.price &&
                Objects.equals(modes, that.modes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modes, price);
    }

    @Override
    public String toString() {
        return "MultiTransfer{" +
                "modes=" + modes +
                ", price=" + price +
                "} " + super.toString();
    }
}
