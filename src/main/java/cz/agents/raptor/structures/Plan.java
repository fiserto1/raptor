package cz.agents.raptor.structures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.agents.raptor.Leg;

import java.io.Serializable;
import java.util.List;

public class Plan implements Serializable {
    private static final long serialVersionUID = 1438972220080286299L;

    @JsonIgnore
    private Stop targetStop;

    private long duration;
    private long price;
    private int numOfTrips;
    private int numOfTransfers;
    private int numOfLegs;
    private List<Leg> legs;

    public Plan(Stop targetStop, long duration, long price, int numOfTrips, int numOfTransfers, int numOfLegs, List<Leg> legs) {
        this.targetStop = targetStop;
        this.duration = duration;
        this.price = price;
        this.numOfTrips = numOfTrips;
        this.numOfTransfers = numOfTransfers;
        this.numOfLegs = numOfLegs;
        this.legs = legs;
    }

    public Stop getTargetStop() {
        return targetStop;
    }

    public void setTargetStop(Stop targetStop) {
        this.targetStop = targetStop;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getNumOfTrips() {
        return numOfTrips;
    }

    public void setNumOfTrips(int numOfTrips) {
        this.numOfTrips = numOfTrips;
    }

    public int getNumOfTransfers() {
        return numOfTransfers;
    }

    public void setNumOfTransfers(int numOfTransfers) {
        this.numOfTransfers = numOfTransfers;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setNumOfLegs(int numOfLegs) {
        this.numOfLegs = numOfLegs;
    }

    public int getNumOfLegs() {
        return numOfLegs;
    }

    @Override
    public String toString() {
        return "Plan{" +
                "targetStop=" + targetStop.getId() +
                ", duration=" + duration +
                ", price=" + price +
                ", numOfTrips=" + numOfTrips +
                ", numOfTransfers=" + numOfTransfers +
                ", numOfLegs=" + numOfLegs +
                '}';
    }
}
