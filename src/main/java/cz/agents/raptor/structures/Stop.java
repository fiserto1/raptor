package cz.agents.raptor.structures;

import cz.agents.raptor.tools.TimeHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a stop, i.e. a point where a trip can be boarded.
 */
public class Stop {
    private String stopId;  // corresponds with GTFS stop ID
    private String stopName;
    private double stopLat; // latitude
    private double stopLon; // longitude

    // routes serving the stop
    private List<Route> stopRoutes = new ArrayList<>();

    // an order of this stop on the route with corresponding index in stopRoutes.
    private List<Integer> stopRoutesIndices = new ArrayList<>();

    // transfers to stops reachable from this stop
    private List<Transfer> transfers = new ArrayList<>();


    public Stop(String stopId, String stopName) {
        this.stopId = stopId;
        this.stopName = stopName;
    }

    public void addRoute(Route route) {
        stopRoutes.add(route);
    }

    public void addIndex(int index) {
        stopRoutesIndices.add(index);
    }

    public String getId() {
        return stopId;
    }

    public String getName() {
        return stopName;
    }

//    public int getArrivalTime(int index) {
//        return stopData.earliestArrivalTimeList.get(index);
//    }
//
//    public int getEarliestArrival() {
//        return stopData.earliestArrivalTime;
//    }
//
//    public Stop getPreviousStop(int index) {
//        return stopData.previousStopList.get(index);
//    }

    public List<Integer> getIndices() {
        return stopRoutesIndices;
    }

    public List<Route> getRoutes() {
        return stopRoutes;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    public double getStopLat() {
        return stopLat;
    }

    public double getStopLon() {
        return stopLon;
    }

//    public Trip getTrip(int index) {
//        return stopData.tripList.get(index);
//    }
//
//    public Transfer getTransfer(int index) {
//        return stopData.transferList.get(index);
//    }
//
//    public void setArrival(int index, int arrivalTime) {
//        stopData.earliestArrivalTimeList.set(index, arrivalTime);
//    }
//
//    public void setEarliestArrival(int earliestArrivalTime) {
//        stopData.earliestArrivalTime = earliestArrivalTime;
//    }

    public void setStopLat(double stopLat) {
        this.stopLat = stopLat;
    }

    public void setStopLon(double stopLon) {
        this.stopLon = stopLon;
    }

//    public void setPreviousStop(int index, Stop stop) {
//        stopData.previousStopList.set(index, stop);
//    }
//
//    public void setTrip(int index, Trip trip) {
//        stopData.tripList.set(index, trip);
//    }
//
//    public void setTransfer(int index, Transfer transfer) {
//        stopData.transferList.set(index, transfer);
//    }
//
//    public void initNewRound() {
//        stopData.earliestArrivalTimeList.add(TimeHelper.INFINITY);
//        stopData.tripList.add(null);
//        stopData.transferList.add(null);
//        stopData.previousStopList.add(null);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stop stop = (Stop) o;

        return stopId.equals(stop.stopId);

    }

    @Override
    public int hashCode() {
        return stopId.hashCode();
    }

    @Override
    public String toString() {
        return "Stop{" +
                "stopId='" + stopId + '\'' +
                ", stopName='" + stopName + '\'' +
                ", stopLat=" + stopLat +
                ", stopLon=" + stopLon +
                '}';
    }
}
