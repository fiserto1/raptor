package cz.agents.raptor.structures;

import cz.agents.geotools.DistanceUtil;
import cz.agents.raptor.tools.TimeHelper;
import cz.agents.raptor.load.gtfs.*;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class represents a timetable composed of a set of routes, stops and transfers and trips.
 */
public class Timetable {
    private static double MAX_TRANSFER_DISTANCE = 500;
    private Map<String, Stop> stops = new HashMap<>();
    private Map<String, Route> routes = new HashMap<>();
    private LocalDate startDate;

    /**
     * Finds whether a service is operating on a given date.
     *
     * @param service a GTFS calendar describing the service
     * @param date    a date to check
     * @return true if a service operates on a given date, false otherwise
     */
    public boolean isOperating(GtfsCalendar service, LocalDate date) {
        if (date.isBefore(service.startDate) || date.isAfter(service.endDate)) {
            return false;
        }
        switch (date.getDayOfWeek()) {
            case MONDAY:
                return service.monday;
            case TUESDAY:
                return service.tuesday;
            case WEDNESDAY:
                return service.wednesday;
            case THURSDAY:
                return service.thursday;
            case FRIDAY:
                return service.friday;
            case SATURDAY:
                return service.saturday;
            case SUNDAY:
                return service.sunday;
            default:
                return false;
        }
    }

    /**
     * Transforms GTFS stops into the stops array.
     *
     * @param gtfsStops stops loaded from a GTFS file.
     */
    private void addStops(Map<String, GtfsStops> gtfsStops) {
        for (GtfsStops gtfsStop : gtfsStops.values()) {
            Stop stop = new Stop(gtfsStop.stopId, gtfsStop.stopName);

            stop.setStopLat(gtfsStop.stopLat);
            stop.setStopLon(gtfsStop.stopLon);

            stops.put(gtfsStop.stopId, stop);
        }
    }

    /**
     * Pairs routes with stops and otherwise.
     *
     * @param route   a route with IDs of stops
     * @param stopIds IDs of stops belonging to the route
     */
    private void addRouteStops(Route route, List<String> stopIds) {
        for (int i = 0; i < stopIds.size(); i++) {
            String stopId = stopIds.get(i);
            Stop stop = stops.get(stopId);

            route.addStop(stop);

            stop.addRoute(route);
            stop.addIndex(i);
        }
    }

    /**
     * Creates new instance of route with a unique route ID and stop IDs.
     * Route is added to the routes array and stops are paired with the new route.
     *
     * @param id        a route id
     * @param gtfsRoute a route loaded from GTFS file
     * @param stopIds   IDs of stops on the route
     * @return a new Route instance
     */
    private Route createNewRoute(String id, GtfsRoutes gtfsRoute, List<String> stopIds) {
        Route route = new Route(gtfsRoute.getRouteId(), gtfsRoute.getRouteShortName(), gtfsRoute.getRouteLongName());

        route.setId(id);
        routes.put(id, route);
        addRouteStops(route, stopIds);

        return route;
    }

    /**
     * Creates a unique key for a route instance.
     * The key is composed of route ID and stop IDs.
     *
     * @param gtfsRoute a route loaded from GTFS file
     * @param stopIds   IDs of stops on the route
     * @return
     */
    private List<String> createUniqueRouteKey(GtfsRoutes gtfsRoute, List<String> stopIds) {
        List<String> key = new ArrayList<>();
        key.add(gtfsRoute.getRouteId());
        key.addAll(stopIds);

        return key;
    }

    /**
     * Creates routes according to GTFS trips and GTFS stop times and adds trips to the routes.
     *
     * @param gtfsTrips          trips loaded from a GTFS file
     * @param gtfsStopTimesArray stop times loaded from a GTFS file
     * @param gtfsCalendars      calendars loaded from a GTFS file
     */
    private void addRoutes(Map<String, GtfsTrips> gtfsTrips, Map<String, List<GtfsStopTimes>> gtfsStopTimesArray,
                           Map<String, GtfsCalendar> gtfsCalendars, Map<String, GtfsRoutes> gtfsRoutes) {

        Map<List<String>, Route> routeSet = new HashMap<>();
        Iterator<GtfsTrips> tripIt = gtfsTrips.values().iterator();
        Iterator<List<GtfsStopTimes>> stopTimesIt = gtfsStopTimesArray.values().iterator();

        while (tripIt.hasNext()) {
            Route route;

            GtfsTrips gtfsTrip = tripIt.next();
            List<GtfsStopTimes> gtfsStopTimes = stopTimesIt.next();
            GtfsCalendar service = gtfsCalendars.get(gtfsTrip.getServiceId());
            GtfsRoutes gtfsRoute = gtfsRoutes.get(gtfsTrip.getRouteId());

            List<String> stopIds = Trip.extractStopIds(gtfsStopTimes);
            List<String> key = createUniqueRouteKey(gtfsRoute, stopIds);

            if (!routeSet.containsKey(key)) {
                String id = String.valueOf(routeSet.size());
                route = createNewRoute(id, gtfsRoute, stopIds);
                routeSet.put(key, route);
            } else {
                route = routeSet.get(key);
            }
            List<Trip> routeTrips = generateTripsByCalendar(gtfsTrip, gtfsStopTimes, service, route);
            route.addTrips(routeTrips);
        }

        // remove routes without trips
        routes.entrySet().removeIf(e->e.getValue().getTrips().isEmpty());

        for (Route route : routes.values()) {
            route.getTrips().sort((o1, o2) -> o1.getDepartureAtStop(0) - o2.getDepartureAtStop(0));
        }
    }

    /**
     * Creates a single instance of a trip for each day the trip operates.
     *
     * @param gtfsTrip      a trip loaded from a GTFS file
     * @param gtfsStopTimes stop times of the trip loaded from a GTFS file
     * @param service       a calendar of the trip loaded from a GTFS file
     */
    private List<Trip> generateTripsByCalendar(GtfsTrips gtfsTrip, List<GtfsStopTimes> gtfsStopTimes, GtfsCalendar service, Route route) {
        LocalDate currentDate = service.startDate;
        LocalDate endDate = service.endDate;

        List<Trip> routeTrips = new ArrayList<>();

        while (!currentDate.isAfter(endDate)) {
            if (isOperating(service, currentDate)) {
                Trip trip = new Trip(gtfsTrip, gtfsStopTimes, TimeHelper.secondsBetween(startDate, currentDate), route);
                routeTrips.add(trip);
            }
            currentDate = currentDate.plusDays(1);
        }
        return routeTrips;
    }

    /**
     * Generates transfers for all stops closer than MAX_TRANSFER_DISTANCE
     */
    private void createTransfersByDistance() {
        for (Stop stop : stops.values()) {
            List<Transfer> transfers = new ArrayList<>();

            for (Stop neighbour : stops.values()) {
                if (stop == neighbour) {
                    continue;
                }
                double distance = getDistanceBetweenStops(stop, neighbour);

                if (distance <= MAX_TRANSFER_DISTANCE) {
                    int timeToTransfer = (int) distance; // 1 meter per second
                    transfers.add(new Transfer(stop, neighbour, timeToTransfer));
                }
            }
            stop.setTransfers(transfers);
        }
    }

    /**
     * Get distance between 2 stops according to their GPS coordinates.
     *
     * @param stop a first stop
     * @param neighbour a second stop
     * @return a distance in meters
     */
    private double getDistanceBetweenStops(Stop stop, Stop neighbour) {
        return DistanceUtil.computeGreatCircleDistance(stop.getStopLat(), stop.getStopLon(), neighbour.getStopLat(), neighbour.getStopLon());
    }

    /**
     * Computes the start date of the timetable.
     * Start date is defined as the earliest start date of any service.
     *
     * @param services calendars loaded from a GTFS file
     */
    private void setStartDate(Map<String, GtfsCalendar> services) {
        LocalDate minDate = LocalDate.MAX;

        for (GtfsCalendar service : services.values()) {
            LocalDate serviceStartDate = service.startDate;

            if (serviceStartDate.isBefore(minDate)) {
                minDate = service.startDate;
            }
        }
        startDate = minDate;
    }

    public static void setMaxTransferDistance(double maxTransferDistance) {
        MAX_TRANSFER_DISTANCE = maxTransferDistance;
    }

    public static double getMaxTransferDistance() {
        return MAX_TRANSFER_DISTANCE;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Stop getStopById(String id) {
        return stops.get(id);
    }

    public Map<String, Stop> getStops() {
        return stops;
    }

    public Map<String, Route> getRoutes() {
        return routes;
    }

    /**
     * Creates a new Timetable instance from a Gtfs instance
     *
     * @param gtfs a Gtfs instance to load the timetable from
     */
    public Timetable(Gtfs gtfs) {
        Map<String, GtfsStops> gtfsStops = gtfs.getStops();
        Map<String, GtfsTrips> gtfsTrips = gtfs.getTrips();
        Map<String, List<GtfsStopTimes>> gtfsStopTimes = gtfs.getStopTimes();
        Map<String, GtfsCalendar> gtfsCalendars = gtfs.getCalendars();
        Map<String, GtfsRoutes> gtfsRoutes = gtfs.getRoutes();

        setStartDate(gtfsCalendars);

        addStops(gtfsStops);
        addRoutes(gtfsTrips, gtfsStopTimes, gtfsCalendars, gtfsRoutes);

        createTransfersByDistance();
    }
}
