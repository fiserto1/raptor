package cz.agents.raptor.tools;

import cz.agents.raptor.Raptor;
import cz.agents.raptor.problems.McProblem;
import cz.agents.raptor.structures.Label;
import cz.agents.raptor.structures.Stop;

import java.util.List;

public class DominationUtils {

    /**
     *
     * @param criteriaA
     * @param criteriaB
     * @return true if A dominates B
     */
    public static boolean dominates(int[] criteriaA, int[] criteriaB) {
        for (int i = 0; i < criteriaA.length; i++) {
            int critA = criteriaA[i];
            int critB = criteriaB[i];

            if (critA > critB) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param bagOfCriteria
     * @param labelB
     * @return true if any label from bag dominates B
     */
    public static boolean anyDominates(List<Label> bagOfCriteria, Label labelB) {
        for (Label labelA : bagOfCriteria) {
            if (labelA == labelB) {
                continue;
            }
            if (dominates(labelA.getCriteria(), labelB.getCriteria())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param bagOfCriteria
     * @param criteriaB
     * @return true if any label from bag dominates B
     */
    public static boolean anyDominates(List<Label> bagOfCriteria, int[] criteriaB) {
        for (Label labelA : bagOfCriteria) {
            if (dominates(labelA.getCriteria(), criteriaB)) {
                return true;
            }
        }
        return false;
    }

    public static boolean anyPrevDominates(McProblem eap, Stop stop, int round, Label routeBagLabel) {
        for (int i = 0; i < round; i++) {
            List<Label> labelBag = eap.getLabelBag(stop, i);
            if (anyDominates(labelBag, routeBagLabel)) {
                return true;
            }
        }
        return false;
    }
}
