package cz.agents.raptor.tools;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * This class is a utility for working with times, dates and LocalDate.
 */
public class TimeHelper {
    public static final int INFINITY = Integer.MAX_VALUE; // max time

    /**
     * Creates a readable date-time string from a base datetime and seconds elapsed from the base datetime.
     *
     * @param baseDate base date represented as an LocalDate instance
     * @param dateTime seconds that have passed since the baseDate
     * @param format ISO format of the string
     * @return a string in readable format representing baseDate + dateTime
     */
    public static String formatDateTime(LocalDate baseDate, int dateTime, String format) {
        if (dateTime == INFINITY) {
            return "null";
        }
        baseDate = baseDate.plusDays(dateTime / (24 * 60 * 60));
        LocalTime localTime = LocalTime.ofSecondOfDay(dateTime % (24 * 60 * 60));
        LocalDateTime localDateTime = LocalDateTime.of(baseDate, localTime);

        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * Creates a single datetime from a base datetime and seconds elapsed from the base datetime.
     *
     * @param baseDate base date represented as an LocalDate instance
     * @param dateTime seconds that have passed since the baseDate
     * @return datetime representing baseDate + dateTime
     */
    public static LocalDateTime getDateTime(LocalDate baseDate, int dateTime) {
        if (dateTime == INFINITY) {
            return null;
        }
        baseDate = baseDate.plusDays(dateTime / (24 * 60 * 60));
        LocalTime localTime = LocalTime.ofSecondOfDay(dateTime % (24 * 60 * 60));
        return LocalDateTime.of(baseDate, localTime);
    }

    /**
     * Creates a readable time string.
     *
     * @param seconds time in seconds (no longer than one day)
     * @param format ISO format of the string
     * @return a string in readable format representing time
     */
    public static String formatTime(int seconds, String format) {
        if (seconds == INFINITY) {
            return "null";
        }
        LocalTime timeOfDay = LocalTime.ofSecondOfDay(seconds);
        return timeOfDay.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * Converts time represented as string to the number of seconds
     *
     * @param timeStr time represented as string
     * @return the number of seconds
     */
    public static int getTimeInSeconds(String timeStr) {
        // Java LocalTime is not used because parsing "pumped-up" time over midnight (e.g. 24:01:30) is needed, too.
        String[] parts = timeStr.split(":");

        int mins = 0;
        if (parts.length > 1) {
            mins = Integer.parseInt(parts[1]);
        }

        int secs = 0;
        if (parts.length > 2) {
            secs = Integer.parseInt(parts[2]);
        }

        return Integer.parseInt(parts[0]) * 3600 + mins * 60 + secs;
    }

    /**
     * Computes a number of seconds between two dates.
     *
     * @param date1 an earlier date
     * @param date2 a later date
     * @return the difference between date1 and date2 in seconds
     */
    public static int secondsBetween(LocalDate date1, LocalDate date2) {
        return (int) ChronoUnit.DAYS.between(date1, date2) * 24 * 60 * 60;
    }
}
