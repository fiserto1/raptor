package cz.agents.raptor.problems;

import cz.agents.raptor.Raptor;
import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.TimeHelper;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EarliestArrivalProblemManchesterTest {
    private Raptor raptor;
    private Timetable timetable;
    private List<Long> results = new ArrayList<>();
    private boolean measure = false;

    private final String GTFS_FILE_PATH = "src/main/resources/timetables/gtfs/TransitFeeds_Manchester_20180815/";

    @Before
    public void setUp() throws Exception {
        Gtfs gtfs = null;
        try {

            gtfs = Gtfs.parseFolder(GTFS_FILE_PATH);
        } catch (ObjectImportException e) {
            e.printStackTrace();
        }
        gtfs.removeIllegalElements();
        System.out.println(gtfs);

        timetable = new Timetable(gtfs);
        raptor = new Raptor();
    }

    private EarliestArrivalProblem createEarliestArrivalProblem(String sourceStr, String destStr, String dateStr, String timeStr) {
        Stop sourceStop = timetable.getStopById(sourceStr);
        Stop targetStop = timetable.getStopById(destStr);
        LocalDate date = new GtfsDateConverter().convertToEntityAttribute(dateStr);
        int departureTime = TimeHelper.getTimeInSeconds(timeStr);

        return new EarliestArrivalProblem(timetable, sourceStop, targetStop, date, departureTime);
    }

    private Journey solveProblem(EarliestArrivalProblem eap) {
        long timeStart = System.currentTimeMillis();
        Journey journey = raptor.solve(eap);
        long timeEnd = System.currentTimeMillis();

        if (measure) {
            results.add(timeEnd - timeStart);
        }

        if (journey != null) {
            journey.print();
        }
        return journey;
    }

    private void checkJourney(Journey journey, int tripCount, int transferCount, String departure, String arrival) {
        assertEquals(journey.getTripCount(), tripCount);
        assertEquals(journey.getTransferCount(), transferCount);

        LocalDate startDate = journey.getTimetableStartDate();
        Trip firstTrip = journey.getTrip(0);
        String departureStr = TimeHelper.formatDateTime(startDate, firstTrip.getDepartureAtStop(journey.getPickUpStop(firstTrip)), "dd.MM.yyyy HH:mm:ss");
        assertEquals(departureStr, departure);

        Trip lastTrip = journey.getTrip(tripCount - 1);
        String arrivalStr = TimeHelper.formatDateTime(startDate, lastTrip.getArrivalAtStop(journey.getDropOffStop(lastTrip)), "dd.MM.yyyy HH:mm:ss");
        assertEquals(arrivalStr, arrival);
    }

    // test from BRADFORD to ORDSALL
    private void testCrossTheCentreSaturday() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("1800EB32101", "1800NF01681", "20181114", "15:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 1, 2, "14.11.2018 15:12:00", "14.11.2018 15:29:00");
    }

    private void testSet() {
        testCrossTheCentreSaturday();
    }

    @Test
    public void solve() throws Exception {
        final int DUMMY_TEST_COUNT = 3;
        final int TEST_COUNT = 25;

        for (int i = 0; i < DUMMY_TEST_COUNT; i++) {
            testSet();
        }

        measure = true;
        for (int i = 0; i < TEST_COUNT; i++) {
            testSet();
        }

        Collections.sort(results);

        OptionalDouble avg = results.stream().mapToLong(Long::longValue).average();

        System.out.println(TEST_COUNT + " rounds of tests passed.");
        System.out.println("Min: " + results.get(0) + " ms");
        System.out.println("Max: " + results.get(results.size()-1)  + " ms");
        System.out.println("Med: " + results.get(results.size() / 2) + " ms");
        System.out.println("Avg: " + avg.getAsDouble() + " ms");
    }
}